import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point

from econtools import load_or_build

from util.env import data_path, src_path
from analysis.geo_exposure import _int_to_xy_multisat, _xy_to_int_multisat

shp_path = src_path('county_shapefiles', 'gz_2010_us_050_00_500k.shp')

# @load_or_build(data_path('counties_grid_count.pkl'))
def counties_grid_count():
    """ This takes forever, just count square km's """
    counties = gpd.read_file(shp_path)
    counties.index = counties['STATE'] + counties['COUNTY']
    out_df = pd.Series(np.zeros(len(counties)), index=counties.index)
    for fips, row in counties.iterrows():
        print(fips)
        bounds = row['geometry'].bounds
        bounds = [_int_to_xy_multisat(_xy_to_int_multisat(x)) for x in bounds]
        bounds[0] -= .01
        bounds[1] -= .01
        bounds[2] += .01
        bounds[3] += .01
        x0, y0, x1, y1 = bounds
        xs = np.arange(x0, x1, .01)
        ys = np.arange(y0, y1, .01)
        for x in xs:
            for y in ys:
                if row['geometry'].contains(Point(x, y)):
                    out_df.at[fips] += 1
    return out_df, counties


if __name__ == "__main__":
    counties = gpd.read_file(shp_path)
    sqkm = counties['CENSUSAREA'] * 2.5899881
