import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from econtools import save_cli

from epa_airpoll import nonattainment_status

from util.env import out_path
from analysis.normed_block_exp import multisat_normed_year


def multisat_normed_hist(year, mon=True, nonattain=None, save=False):
    norm_year = year
    plot_year = year
    df = multisat_normed_year(norm_year, plot_year)

    samp = df[df[f'{plot_year}_exp_monnorm_{norm_year}'].notnull()]

    samp = samp.join(prep_nonattain().to_frame('nonattain'), on='fips')
    samp['nonattain'] = samp['nonattain'].fillna(False)

    if mon:
        pass
    else:
        samp = samp.loc[~samp['has_mon'], :]

    fig, ax = plt.subplots()

    ax.set_xlabel("Normalized Concentration")
    # ax.yaxis.grid(color='gray', alpha=0.5, zorder=9)
    ax.set_ylabel("Density")

    if nonattain is None:
        plot_df = samp
    elif nonattain:
        plot_df = samp[samp['nonattain']]
    else:
        plot_df = samp[~samp['nonattain']]
    plot_var = f'{plot_year}_exp_monnorm_{norm_year}'

    # Define bins
    step = 0.25
    samp_min = samp[plot_var].min()     # Not `plot_df` so shared axes
    samp_max = samp[plot_var].max()
    bin0 = 1.125
    while bin0 > samp_min:
        bin0 -= step
    bins = np.arange(bin0, samp_max + step, step)

    plot_df['bins'] = pd.cut(plot_df[plot_var], bins, labels=bins[1:]-.125)
    hist = plot_df.groupby('bins')['pop'].sum()

    ax.bar(bins[:-1], hist.values / hist.sum(), step, align='edge',
           color='navy')

    ticks = np.arange(bin0 + step / 2, samp_max + step / 2, step)
    ax.set_xticks(ticks)
    ax.set_xticklabels(ticks, rotation=50)

    if save:
        path_root = f'hist_multisat_normed_{year}'
        if nonattain:
            path_root += '_nonattain'
        elif not nonattain and nonattain is not None:
            path_root += '_attain'
        path_root += '.pdf'
        fig.savefig(out_path(path_root), bbox_inches='tight')
        plt.close()
    else:
        plt.show()

    return samp


def prep_nonattain():
    df = nonattainment_status()['pm25_12'][2015]
    df.index.name = 'fips'
    return df


if __name__ == '__main__':
    year = 2015

    df = multisat_normed_hist(year, save=save_cli())
    # df = multisat_normed_hist(2015, nonattain=True, save=save)
    # df = multisat_normed_hist(2015, nonattain=False, save=save)
