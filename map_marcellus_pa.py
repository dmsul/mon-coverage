import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.feature import ShapelyFeature
from cartopy.io.shapereader import Reader

from econtools import save_cli

from drillinginfo.raw import clean_wells, marcellus_shape

from map_aod_pa import get_pa_shape

from epa_airpoll.clean.raw import monitors_data
from epa_airpoll.util import name_to_fips_xwalk
from epa_airpoll.util.env import src_path, out_path


def marcellus_wells_pa(pollutant, year, save=False):
    # state boundaries
    state_path = src_path('census', 'gis', 'cb_2016_us_state_5m',
                          'cb_2016_us_state_5m.shp')
    states = ShapelyFeature(Reader(state_path).geometries(),
                            ccrs.PlateCarree())
    ax = plt.axes(projection=ccrs.PlateCarree())
    ax.add_feature(states, facecolor='none', edgecolor='black', linewidth=1,
                   zorder=2)

    # map boundaries
    x0, y0, x1, y1 = get_pa_shape().bounds
    bbox_buffer = 0.75  # zoom out a bit
    x0 = x0 - bbox_buffer
    y0 = y0 - bbox_buffer
    x1 = x1 + bbox_buffer
    y1 = y1 + bbox_buffer

    # marcellus formation
    shape = marcellus_shape()
    ax.set_extent([x0, x1, y0, y1], ccrs.PlateCarree())
    ax.add_feature(shape, facecolor='bisque', edgecolor='none', zorder=1)

    # oil & gas wells
    wells = clean_wells()
    wells = wells[wells['target_formation'] == 'MARCELLUS']
    x_wells = wells['x']
    y_wells = wells['y']
    plt.scatter(x_wells, y_wells, facecolor='brown', edgecolor='black',
                linewidth='.5', zorder=3, s=2, marker='o')

    # pennsylvania monitors
    monitors = monitors_data()
    pa_mon = monitors['state_code'] == int(name_to_fips_xwalk['Pennsylvania'])
    monitors = monitors[pa_mon]
    monitors = monitors[monitors['parameter_code'] == pollutant]

    # naaqs vs. non-naaqs
    naaqs = monitors[monitors['naaqs_primary_monitor'] == 'Y']
    naaqs = active_monitors(naaqs, year)
    x_naaqs = naaqs['longitude']
    y_naaqs = naaqs['latitude']
    plt.scatter(x_naaqs, y_naaqs, facecolor='blue', edgecolor='none',
                zorder=5, s=8, marker='^')

    not_naaqs = monitors[monitors['naaqs_primary_monitor'] != 'Y']
    not_naaqs = active_monitors(not_naaqs, year)
    x_not_naaqs = not_naaqs['longitude']
    y_not_naaqs = not_naaqs['latitude']
    plt.scatter(x_not_naaqs, y_not_naaqs, facecolor='red', edgecolor='none',
                zorder=4, s=8, marker='^')

    # legend
    colors = ['bisque', 'brown', 'blue', 'red']
    markers = ['s', 'o', '^', '^']
    texts = ["Marcellus Formation",
             "Oil & Gas Wells",
             "NAAQS Monitors",
             "non-NAAQS Monitors"]
    patches = [plt.plot([], [], marker=markers[i], markersize=7, linestyle='',
                        color=colors[i], label="{:s}".format(texts[i]))[0]
               for i in range(len(texts))]
    plt.legend(handles=patches, loc='upper left', fontsize='x-small',
               edgecolor='black', ncol=2, facecolor='white')

    ax.outline_patch.set_edgecolor('none')

    # saving
    if save:
        plt.savefig(out_path(f'map_marcellus_pa_{pollutant}_{year}.png'),
                    dpi=600, bbox_inches='tight')
        plt.close()
    else:
        plt.show()


def active_monitors(df, year):
    df = df[(df['start_date'].dt.year < year) &
            (df['end_date'].dt.year > year)]
    return df


if __name__ == '__main__':
    save = save_cli()
    marcellus_wells_pa(88101, 2010, save=save)
