#! /bin/bash

python plot_hist_normed_exp.py --save

python map_exposure.py conus --save
python map_exposure.py hotspot --save

python map_attainment.py --save
python map_misclassed.py --save

# python plot_hist_exp_by_attain.py 2005 --rule pm25_97 --save
# python plot_hist_exp_by_attain.py 2015 --rule pm25_12 --save

# python plot_ts_overexposed.py --save
# python ./plot_ts_sat.py --rule pm25_12 --data msatna --split --save
python ./plot_ts_monitor.py --save

python reg_nonattain.py --save
python reg_es.py --save

python calc_mortality.py --save

python summ_nonatt_demo.py --rule pm25_12 --year 2015 --save
python ./summ_misclass_cause.py --save
