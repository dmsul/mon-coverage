import matplotlib.pyplot as plt

from analysis.monitor_sample import (semi_constant_monitor_panel,
                                     prep_monitor_analysis)

if __name__ == "__main__":
    rule = 'pm25_12'
    df = prep_monitor_analysis(semi_constant_monitor_panel(rule=rule),
                               rule=rule)
    df = df[~(df['monitor_id'] == '06031_4_881011')]
    nonatt = df[df['untargeted']]

    by_mon = nonatt.groupby('monitor_id')

    fig, ax = plt.subplots()
    for idx, mon in by_mon:
        ax.plot(mon['year'], mon['arithmetic_mean'], '-o',
                label=idx,
                )

    plt.show()
