import numpy as np

from epa_airpoll import nonattainment_status

if __name__ == "__main__":
    df = nonattainment_status()
    df.columns.names = ['poll', 'year']
    # Find first year of attainment status
    double_stack = (df
                    .stack('poll')
                    .stack('year')
                    .to_frame('nonatt')
                    .reset_index())
    is_true = double_stack[double_stack['nonatt']]
    first_year = is_true.groupby('poll')['year'].min()
    double_stack['nonatt'] = double_stack['nonatt'].astype(int)
    for poll, year in first_year.iteritems():
        double_stack.loc[
            (double_stack['poll'] == poll) &
            (double_stack['year'] < year),
            'nonatt'] = np.nan

    df = double_stack.set_index(['county', 'year', 'poll']).unstack('poll')
    diff = df.groupby('county').diff().stack('poll').squeeze()
    reclassed = diff[diff > 0].reset_index().sort_values(['poll', 'year'])
    print(reclassed)
