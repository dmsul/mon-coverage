import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from epa_airpoll import nonattainment_block_panel

from util.env import out_path
from analysis.geo_exposure import multisatpm_exposure_block_conus
from analysis.basic_data import (merge_blocks_pop, block_has_monitor,
                                 prep_multisatpm_3year_wlag_block,
                                 msatna_blocks_panel,
                                 msatna_blocks_3lag_year)


def main(year, data='multisatpm', rule=None, nonattain_year=None, save=False):
    df = data_prep(year, rule=rule, nonattain_year=nonattain_year, data=data)
    attain_nomon = df[(~df['nonattain'])]
    nonattain_mon = df[(df['nonattain']) & (df['county_has_mon'])]

    type1 = attain_nomon.groupby('exposure')['pop'].sum()
    type2 = nonattain_mon.groupby('exposure')['pop'].sum()

    if data in ('msatna3', 'multisatpm3'):
        type1.index = np.around(type1.index)
        type2.index = np.around(type2.index)
        type1 = type1.groupby(level=0).sum()
        type2 = type2.groupby(level=0).sum()

    f1 = type1 / type1.sum()
    f2 = type2 / type2.sum()

    fig, ax = plt.subplots()

    ax.step(f1.index + .5, f1.values, color='navy', alpha=.8,
            label='Attainment')
    ax.step(f2.index + .5, f2.values, color='maroon', alpha=.8,
            label='Non-attainment, has monitor')

    ax.set_ylim(bottom=0)
    ax.set_xticks(
        list(
            range(3,
                  int(max(f2.index.max(), f1.index.max())) + 2,
                  3)
        )
    )

    ax.yaxis.grid(color='gray', alpha=0.5, zorder=9)
    ax.set_ylabel("Density")
    ax.set_xlabel(
        "Satellite-measured PM$_{2.5}$ Concentration ($\mu g/m^3$)")

    cutoff = 12 if rule == 'pm25_12' else 15
    ax.axvline(cutoff,
               color='g', linestyle='--', zorder=2, label='NAAQS Limit', lw=1)

    # Shrink axes for legend
    box = ax.get_position()
    ax.set_position([box.x0,
                     box.y0 + box.height * 0.15,
                     box.width,
                     box.height * 0.85])

    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=2)

    if save:
        filename = f'hist_exposure_byattain_{data}_{rule}_{year}.pdf'
        fig.savefig(out_path(filename), bbox_inches='tight')
        plt.close()
    else:
        plt.show()

    return df


def data_prep(year, rule=None, nonattain_year=None, data='multisatpm'):
    if data == 'multisatpm':
        df = multisatpm_exposure_block_conus(year)
    elif data == 'msatna':
        df = msatna_blocks_panel()[year].to_frame('exposure')
    elif data == 'msatna3':
        df = msatna_blocks_3lag_year(year).to_frame('exposure')
    elif data == 'multisatpm3':
        df = prep_multisatpm_3year_wlag_block()[year].to_frame('exposure')
    else:
        raise NotImplementedError(f"Data {data} no yet")

    if not rule:
        rule = 'pm25_97' if year < 2015 else 'pm25_12'

    if not nonattain_year:
        nonattain_year = year

    if type(df) is pd.Series:
        df = df.to_frame()
    df = merge_blocks_pop(df)
    has_mon = block_has_monitor(year)
    df = df.join(has_mon)
    df['county'] = df.index.str[:5]
    nonattain = nonattainment_block_panel(rule)[nonattain_year]
    df = df.join(nonattain.to_frame('nonattain'))
    df['nonattain'] = df['nonattain'].fillna(False)

    county_has_mon = df.groupby('county')['has_mon'].max()
    df = df.join(county_has_mon.to_frame('county_has_mon'), on='county')

    return df


if __name__ == '__main__':
    import argparse

    opts = argparse.ArgumentParser()
    opts.add_argument('year', type=int)
    opts.add_argument('--rule', type=str, default='',
                      choices=['pm25_97', 'pm25_12', 'pm25_06'])
    opts.add_argument('--nonattain-year', type=int, default=None)
    opts.add_argument('--data', type=str, default='multisatpm')
    opts.add_argument('--save', action='store_true')

    args = opts.parse_args()

    df = main(args.year, data=args.data, rule=args.rule,
              nonattain_year=args.nonattain_year, save=args.save)

    df2 = df.loc[~df['nonattain']]
    pop_totals = (df2.groupby(['exposure', 'county_has_mon'])['pop']
                  .sum()
                  .unstack('county_has_mon'))
    pop_totals['total'] = pop_totals.sum(axis=1)

    for cutoff in range(12, 17):
        print(f"Attainment at or above {cutoff}:")
        print("\t{:,}".format(int(pop_totals.loc[cutoff:, 'total'].sum())))
