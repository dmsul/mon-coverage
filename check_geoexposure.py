import pandas as pd
import cartopy.crs as ccrs
import matplotlib.pyplot as plt

from epa_airpoll.util import name_to_fips_xwalk, fips_to_name_xwalk
from epa_airpoll.clean.census.shapefiles import load_bg_shape

from modis.clean.raw import load_modis_year
from modis.util import annual_mean

from analysis.geo_exposure import state_modis_exposure_bg


def check_geoexposure(modis_year, test_block_group):
    # MODIS
    modis_data = load_modis_year(modis_year).reset_index()

    # CONUS block groups
    states = name_to_fips_xwalk.keys()
    state_bg = [(load_bg_shape(name_to_fips_xwalk[state])) for state in states]
    conus_bg = pd.concat(state_bg)
    del state_bg

    conus_bg['bg_id'] = (conus_bg['STATE'].astype(str).str.zfill(2) +
                         conus_bg['COUNTY'].astype(str).str.zfill(3) +
                         conus_bg['TRACT'].astype(str).str.zfill(6) +
                         conus_bg['BLKGRP'].astype(str).str.zfill(1))
    conus_bg = conus_bg.set_index('bg_id')

    # block group subset
    conus_bg = conus_bg.reset_index()
    bg_draw = test_block_group
    bg = conus_bg.loc[conus_bg.index == bg_draw, :]

    # bounding box
    bbox = bg.bounds
    bound_columns = ['x0', 'y0', 'x1', 'y1']
    bbox.columns = bound_columns
    xy0 = ['x0', 'y0']
    xy1 = ['x1', 'y1']
    bbox_buffer = 0.05
    bbox[xy0] = bbox[xy0] - bbox_buffer
    bbox[xy1] = bbox[xy1] + bbox_buffer

    bg = bg.join(bbox, how='left')

    bg_x0 = bg['x0'].squeeze()
    bg_x1 = bg['x1'].squeeze()
    bg_y0 = bg['y0'].squeeze()
    bg_y1 = bg['y1'].squeeze()

    in_bg_bounds = ((modis_data['x'] > bg_x0) &
                    (modis_data['x'] < bg_x1) &
                    (modis_data['y'] > bg_y0) &
                    (modis_data['y'] < bg_y1)
                    )

    # check for block groups with no nearby MODIS points
    if in_bg_bounds.empty:
        raise ValueError((f"Block Group {test_block_group} has no nearby MODIS"
                          f"pixels in {modis_year}"))
    else:
        pass

    bg_modis = modis_data[in_bg_bounds]
    mean = annual_mean(bg_modis)
    bg['mean_modis'] = mean

    # comparison
    df = state_modis_exposure_bg(fips_to_name_xwalk[bg['STATE'].squeeze()],
                                 modis_year, modis_data=modis_data)
    df = df.to_frame()
    comparison_mean = df.loc[bg['bg_id'], :]

    try:
        assert bg['mean_modis'].squeeze() == comparison_mean.squeeze()
    except AssertionError:
        errstr = '!!! MODIS means different !!!\nNew: {}\nOld: {}'
        print(errstr.format(bg['mean_modis'], comparison_mean))

    # mapping
    crs = ccrs.PlateCarree()
    ax = plt.axes(projection=crs)
    ax.set_extent([bg_x0, bg_x1, bg_y0, bg_y1], crs)

    crs_proj4 = crs.proj4_init
    bg = bg.to_crs(crs_proj4)
    ax.add_geometries(bg['geometry'], zorder=1, crs=crs)

    x = bg_modis['x']
    y = bg_modis['y']
    plt.scatter(x, y, facecolor='brown', edgecolor='black', linewidth='.5',
                zorder=2, marker='o')

    ax.set_title(f'Block Group {bg_draw}; MODIS average = {mean}')

    plt.show()

    return bg_modis, bg


if __name__ == '__main__':
    bg_modis, bg = check_geoexposure(2010, 155)
