import numpy as np
import pandas as pd
import geopandas as gpd
from cartopy.io.shapereader import Reader
import cartopy.crs as ccrs
from cartopy.mpl.patch import geos_to_path
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib as mpl
from matplotlib.patches import Patch

from econtools import state_abbr_to_name, state_name_to_fips

from epa_airpoll import nonattainment_shape

from util import conus_bounds
from util.env import src_path, out_path, gis_src_path
from analysis.basic_data import msatna_3lag_year
from analysis.misclass import fips_misclass_flag


def main(exp_year: int, rule: str, data: str,
         state: str='',
         web_color: bool=False,
         save: bool=False) -> None:

    misclass_color = 'maroon'
    if web_color:
        nonatt_color = '#FFC2C1'
        has_misclass_color = '#FF6663'
        county_plain_color = '#BFD7EA'
    else:
        nonatt_color = 'k'
        has_misclass_color = 'orange'
        county_plain_color = '0.9'

    crs = ccrs.PlateCarree()

    ax = plt.axes(projection=crs)   # Initialize axis

    ax, county_df = load_and_plot_county_borders(ax, crs, county_plain_color,
                                                 state=state)

    # Plot counties that contain "attain & over NAAQS" blocks
    has_misclass = fips_misclass_flag(exp_year, rule, data)
    has_misclass_gdf = county_df.join(has_misclass)
    has_misclass_gdf = has_misclass_gdf.fillna({'fips_misclass': False})
    has_misclass_gdf = has_misclass_gdf[has_misclass_gdf['fips_misclass']]
    ax.add_geometries(has_misclass_gdf['geometry'],
                      color=has_misclass_color, edgecolor='none',
                      linewidth=0.1, crs=crs,
                      zorder=2,
                      )

    # Plot misclassed *1km squares* like concentrations (binary) b/c loading
    # block shapes takes forever
    if 0:
        df = block_is_over_naaqs(exp_year, rule, data)

        if state:
            df, boundary_shape = restrict_df_to_state(df, state)
        else:
            df, boundary_shape = restrict_df_to_conus(df)

        df = df.set_index(['y', 'x']).squeeze()
        df3 = df.unstack('x')
        pollutant = np.ma.masked_array(df3.values, np.isnan(df3.values))
        x, y = np.meshgrid(df3.columns, df3.index)

        norm = mpl.colors.BoundaryNorm(boundaries=[-1, .5, 1.5], ncolors=2)
        cmap = mpl.colors.ListedColormap(['none', misclass_color])

        plot = ax.pcolormesh(
            x, y, pollutant, transform=ccrs.PlateCarree(),
            cmap=cmap,
            norm=norm,
            zorder=3,
        )
    else:
        if state:
            boundary_shape = get_state_shape(state)
        else:
            boundary_shape = _load_conus_shape()

    if state:
        x0, y0, x1, y1 = boundary_shape.bounds
    else:
        x0, x1, y0, y1 = conus_bounds
    ax.set_extent([x0, x1, y0, y1], ccrs.PlateCarree())

    # Plot nonattain shapefiles
    nonatt_year = 2015 if rule == 'pm25_12' else 2005
    shp = nonattainment_shape(nonatt_year)
    if state:
        shp = shp[shp['state_abbr'] == state]
    plot = ax.add_geometries(shp['geometry'],
                             color=nonatt_color, edgecolor='none',
                             linewidth=0.5, crs=crs,
                             zorder=4)

    # Re-plot countylines to go over everything else
    ax.add_geometries(county_df['geometry'],
                      color='none', edgecolor='white',
                      linewidth=0.1, crs=crs,
                      zorder=5,
                      )
    # Clip
    trans = ccrs.PlateCarree()._as_mpl_transform(ax)
    plot.set_clip_path(
        Path.make_compound_path(*geos_to_path(boundary_shape)),
        transform=trans
    )

    # Draw state boundaries thicker than county boundaries
    if not state:
        shp_path = gis_src_path('census', 'cb_2016_us_state_5m',
                                'cb_2016_us_state_5m.shp')
        adm_shapes = list(Reader(shp_path).geometries())
        ax.add_geometries(adm_shapes, ccrs.PlateCarree(),
                          edgecolor='w', facecolor='none', linewidth=0.3,
                          alpha=1,
                          zorder=9)

    # Legend
    if state or not web_color:
        legend_elements = [
            Patch(color=nonatt_color,
                  label='Nonattainment'),
            Patch(color=county_plain_color,
                  label='Attainment'),
            Patch(color=has_misclass_color,
                  label='Attainment but misclassified'),
        ]
        ax.legend(handles=legend_elements, loc='lower left', fontsize=6)

    ax.outline_patch.set_edgecolor('none')
    ax.outline_patch.set_visible(False)
    ax.background_patch.set_visible(False)

    if save:
        if state:
            filepath = out_path(
                f'map_misclassed_{rule}_{data}_{exp_year}_{state}.pdf')
        else:
            filepath = out_path(f'map_misclassed_{rule}_{data}_{exp_year}.png')
        plt.savefig(
            filepath,
            dpi=500,
            bbox_inches='tight',
            transparent=True,
        )
        plt.close()
    else:
        plt.show()

def block_is_over_naaqs(exp_year: int, rule: str, data: str) -> pd.DataFrame:
    """ Bool for 'block is over naaqs' """
    # Core data
    if data == 'msatna':
        df = msatna_3lag_year(exp_year)
    elif data == 'multisatpm':
        raise NotImplementedError

    # Contru
    naaqs = 12 if rule == 'pm25_12' else 15
    df[df < naaqs] = np.nan
    df[df.notnull()] = 1
    df.name = 'pm25'
    df = df.reset_index()

    return df

def restrict_df_to_conus(df: pd.DataFrame) -> pd.DataFrame:
    x0, x1, y0, y1 = conus_bounds
    df = df[
        (df['x'] > x0) & (df['x'] < x1) &
        (df['y'] > y0) & (df['y'] < y1)
    ].copy()

    boundary_shape = _load_conus_shape()

    return df, boundary_shape

def restrict_df_to_state(df: pd.DataFrame, abbr: str) -> pd.DataFrame:
    boundary_shape = get_state_shape(abbr)
    x0, y0, x1, y1 = boundary_shape.bounds
    df = df[
        (df['x'] > x0) & (df['x'] < x1) &
        (df['y'] > y0) & (df['y'] < y1)
    ].copy()

    return df, boundary_shape


def _load_conus_shape():
    boundary_shape = list(Reader(src_path(
        'cb_2016_us_nation_5m', 'cb_2016_us_nation_5m.shp'
    )).records())[0].geometry
    return boundary_shape


def get_state_shape(abbr):
    state_name = state_abbr_to_name(abbr)
    shp_path = gis_src_path(
        'census', 'cb_2016_us_state_5m', 'cb_2016_us_state_5m.shp')
    df = gpd.read_file(shp_path)
    return df.loc[df['NAME'] == state_name, 'geometry'].squeeze()

def load_and_plot_county_borders(ax, crs, facecolor, state=''):
    shp_path = src_path('county_shapefiles',
                        'gz_2010_us_050_00_500k.shp')

    county_df = gpd.read_file(shp_path)

    if state:
        county_df = county_df[
            county_df['STATE'] ==
            str(state_name_to_fips(state_abbr_to_name(state))).zfill(2)
        ]

    county_df = county_df.to_crs(crs.proj4_init)
    county_df.index = county_df['STATE'] + county_df['COUNTY']
    county_df.name = 'fips'

    ax.add_geometries(county_df['geometry'],
                      color=facecolor, edgecolor='white',
                      linewidth=0.005, crs=crs,
                      zorder=1,
                      )

    return ax, county_df


if __name__ == "__main__":
    import argparse

    opts = argparse.ArgumentParser()
    opts.add_argument('--year', type=int, default=2014)
    opts.add_argument('--rule', type=str, default='pm25_12')
    opts.add_argument('--data', type=str, default='msatna',
                      choices=['multisatpm', 'msatna'])
    opts.add_argument('--state', type=str, default='')
    opts.add_argument('--webcolor', action='store_true')
    opts.add_argument('--save', action='store_true')
    args = opts.parse_args()

    main(args.year, args.rule, args.data,
         state=args.state,
         web_color=args.webcolor,
         save=args.save)
