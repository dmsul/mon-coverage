import pandas as pd
import geopandas as gpd
import cartopy.crs as ccrs
import matplotlib.pyplot as plt

from epa_airpoll import (nonattainment_status, load_block_shape,
                         nonattainment_block)
from epa_airpoll.clean.nonattainment_subcounty import nonattainment_shape

from util.env import src_path, out_path

from econtools import save_cli

shp_path = src_path('county_shapefiles', 'gz_2010_us_050_00_500k.shp')

crs = ccrs.PlateCarree()


def map_nonatt_blocks(rule='pm25_12', year=2015, save=False):
    counties = gpd.read_file(shp_path)

    # All and non-attainment counties
    counties.index = (counties['STATE'].astype(str).str.zfill(2) +
                      counties['COUNTY'].astype(str).str.zfill(3))
    df = nonattainment_status()
    df = df.loc[:, pd.IndexSlice[rule, year]].to_frame('nonattain')
    df = counties.join(df, how='left').fillna(False)
    crs_proj4 = crs.proj4_init
    df = df.to_crs(crs_proj4)

    nonatt_block = nonattainment_block(year).to_frame('nonatt')
    nonatt_block = nonatt_block.loc[nonatt_block['nonatt']]
    nonatt_block['fips'] = nonatt_block.index.str[:5]
    nonatt_counties = (nonatt_block['fips'].drop_duplicates()).tolist()

    for fips in nonatt_counties:
        print(fips)
        df_new = df.loc[[fips], :].copy()

        fig, ax = plt.subplots()
        ax = plt.axes(projection=ccrs.PlateCarree())
        ax.add_geometries(df_new['geometry'],
                          color='0.5', edgecolor='white', linewidth=0.05,
                          crs=crs)

        # plot nonattainment blocks
        blocks = prep_nonatt_blocks(fips[:2], year)
        blocks = blocks[blocks.index.str[:5] == fips].copy()
        ax.add_geometries(blocks['geometry'],
                          color='red', crs=crs)

        # plot nonattainment shapes (i.e., sometimes only parts of blocks)
        nonatt_shape = nonattainment_shape(year)
        ax.add_geometries(nonatt_shape.loc[:, 'geometry'],
                          facecolor='none', edgecolor='green', crs=crs)

        # set extent to nonattainment block
        # XXX this should account for having mapped all 9 nonattainment shapes
        b = df_new.total_bounds
        bounds = [b[0], b[2], b[3], b[1]]
        ax.set_extent(bounds, ccrs.PlateCarree())

        ax.outline_patch.set_edgecolor('none')

        # export figs
        if save:
            fig.savefig(out_path(f'tmp_check_nonatt_{fips}_{year}.png'),
                        bbox_inches='tight')
            plt.close()
        else:
            plt.show()


def prep_nonatt_blocks(fips, year):
    df = load_block_shape(fips)[['GEOID10', 'geometry']]
    df = df.rename(columns={'GEOID10': 'block_id'})
    df = df.set_index('block_id')
    nonatt_block = nonattainment_block(year).to_frame('nonatt')
    df = df.join(nonatt_block, how='inner')
    df = df[df['nonatt'].fillna(False)]

    return df


if __name__ == '__main__':
    map_nonatt_blocks('pm25_12', 2015, save=save_cli())
