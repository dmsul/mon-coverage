import numpy as np
import pandas as pd

from econtools import load_or_build, table_statrow, save_cli

from epa_airpoll import monitors_annual_summary

from util.env import data_path, out_path
from analysis.monitor_sample import valid_naaqs_monitors


def main(lag3: bool=False, save: bool=False) -> None:
    year0 = 2013 if lag3 else 2010
    yearT = 2017 if lag3 else 2016

    table_str = ''
    for year in range(year0, yearT + 1):
        df = prep_data(year, lag3=lag3)
        cross = pd.crosstab(df['is_hourly'], df['coverage_cat'])
        total = cross.sum()

        try:
            frac_hourly = cross.loc[True] / total
            total_hourly = cross.loc[True].sum()
        except KeyError:
            frac_hourly = np.arange(4) * 0
            total_hourly = 0

        if 0:
            vals = (total / total.sum()).tolist()
            vals = [str(int(np.around(x * 100))) + '\\%' for x in vals]
        else:
            vals = total.tolist()
        vals.append(total.sum())

        table_str += table_statrow(str(year), vals, digits=0)

        # Add row of "fraction that are hourly"
        if 0:
            vals2 = frac_hourly.tolist()
            vals2.append(total_hourly / total.sum())
            vals2 = [str(int(np.around(x * 100))) + '\\%' for x in vals2]
            table_str += table_statrow('', vals2, sd=True)

    print(table_str)

    if save_cli():
        fileroot = 'summ_monitor_freq'
        if lag3:
            fileroot += '_lag3'
        fileroot += '.tex'
        with open(out_path(fileroot), 'w') as f:
            f.write(table_str)


@load_or_build(data_path('tmp_summ_mon_{year}_{lag3}.pkl'))
def prep_data(year: int, lag3: bool=True) -> pd.DataFrame:
    df = (valid_naaqs_monitors(year, lag3=lag3)
          .drop_duplicates()
          .set_index('monitor_id'))
    assert df.index.is_unique

    if year == 2008 and not lag3:
        df = df.drop('06045_6_881013', axis=0)  # On for 1 hour!!

    use_year = year - 1 if lag3 else year
    df2 = prep_mon_summary(use_year)
    df2 = gen_mon_coverage_cat(df2)
    coverage_cat = df2.set_index('monitor_id')[['coverage_cat', 'is_hourly']]
    df = df.join(coverage_cat)

    assert df['coverage_cat'].notnull().min()

    return df


# XXX copy-paste-edit from cv_universal
def prep_mon_summary(year: int) -> pd.DataFrame:
    df = monitors_annual_summary(year)
    df = df[df['parameter_code'] == 88101]
    df = df[df['event_type'].isin(('No Events', 'Events Included'))]
    df = df[df['pollutant_standard'] == 'PM25 Annual 2006']

    df = df.rename(columns={'arithmetic_mean': 'mean'})

    assert df.shape == df.drop_duplicates('monitor_id').shape

    return df


def gen_mon_coverage_cat(df: pd.DataFrame) -> pd.DataFrame:
    df['coverage_cat'] = -1
    df.loc[df['valid_day_count'] <= 80, 'coverage_cat'] = 1
    df.loc[(df['valid_day_count'] > 80) &
           (df['valid_day_count'] <= 120), 'coverage_cat'] = 2
    df.loc[(df['valid_day_count'] > 120) &
           (df['valid_day_count'] <= 300), 'coverage_cat'] = 3
    df.loc[df['valid_day_count'] > 300, 'coverage_cat'] = 4

    df['is_hourly'] = df['sample_duration'] == '24-HR BLK AVG'

    assert (df['coverage_cat'] > 0).min()

    return df


if __name__ == "__main__":
    save = save_cli()
    main(lag3=False, save=save)
    main(lag3=True, save=save)
