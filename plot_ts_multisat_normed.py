import pandas as pd
import matplotlib.pyplot as plt

from analysis.normed_block_exp import multisat_normed_panel

from epa_airpoll.clean.nonattainment_status import nonattainment_status


pm_implementation_xwalk = {1997: 2005,
                           2006: 2009,
                           2012: 2015}


pm_rule_xwalk = {1997: 'pm25_97',
                 2006: 'pm25_06',
                 2012: 'pm25_12'}


def multisatpm_exposure_plot(rule_year, start_year, end_year):
    df = prep_data(rule_year, start_year, end_year)

    # monitor and attainment groups
    df_plot = df.groupby(['cat', 'year'])['exposure_weighted'].sum()

    mon_att = df_plot.loc[pd.IndexSlice[10, :]].reset_index()
    mon_non = df_plot.loc[pd.IndexSlice[11, :]].reset_index()
    nomon_att = df_plot.loc[pd.IndexSlice[0, :]].reset_index()
    nomon_non = df_plot.loc[pd.IndexSlice[1, :]].reset_index()

    # plot
    fig, ax = plt.subplots()

    ax.plot(mon_att['year'], mon_att['exposure_weighted'],
            '-o', linestyle='--', label="Monitor - Attainment",
            color='navy', zorder=9)

    ax.plot(nomon_att['year'], nomon_att['exposure_weighted'],
            '-o', linestyle='-', label="No Monitor - Attainment",
            color='navy', zorder=9)

    ax.plot(mon_non['year'], mon_non['exposure_weighted'],
            '-^', linestyle='--', label="Monitor - Nonattainment",
            color='maroon', zorder=9)

    ax.plot(nomon_non['year'], nomon_non['exposure_weighted'],
            '-^', linestyle='-', label="No Monitor - Nonattainment",
            color='maroon', zorder=9)

    ax.set_xticks(mon_att['year'])
    ax.set_xticklabels(mon_att['year'].astype(int), rotation=50)

    ax.axvline(pm_implementation_xwalk[rule_year],
               color='forestgreen', linestyle='-.', zorder=2)

    ax.yaxis.grid(color='gray', alpha=0.5, zorder=1)
    ax.set_ylabel("Mean PM$_{2.5}$ ($\mu$g/m$^3$)")

    ax.set_title("Population-weighted Exposure")
    ax.legend(loc='best', fontsize='x-small',
              edgecolor='black', handlelength=3)

    plt.show()


def prep_data(rule_year, start_year, end_year):
    df = multisat_normed_panel(rule_year, start_year, end_year)
    df['county'] = df.index.astype(str).str[0:5]

    # attainment status
    nonattain = nonattainment_status()
    nonattain = (
        nonattain['pm25_97'][rule_year].to_frame('nonattain')
    )
    nonattain.index.name = 'county'

    df = df.join(nonattain, how='left', on='county')
    df['nonattain'] = df['nonattain'].fillna(False)

    # assign attainment & monitor status
    df['cat'] = -1
    df.loc[(df['has_mon']) & (~df['nonattain']), 'cat'] = 10
    df.loc[df['has_mon'] & (df['nonattain']), 'cat'] = 11
    df.loc[~df['has_mon'] & (~df['nonattain']), 'cat'] = 0
    df.loc[~df['has_mon'] & (df['nonattain']), 'cat'] = 1
    assert (df['cat'] != -1).min()

    # population weights
    cat_pop_sum = df.groupby('cat')['pop'].sum()  # pop within each cat group
    df = df.join(cat_pop_sum.to_frame('cat_pop_sum'), on='cat')
    df['weight'] = df['pop'] / df['cat_pop_sum']

    # wide to long
    df = df.drop(['x', 'y', 'x_int', 'y_int', 'has_mon', 'fips', 'cat_pop_sum',
                  'nonattain', f'{rule_year}_exp_at_mon'], axis=1)

    years = range(start_year, end_year + 1)
    for year in years:
        df = df.drop(f'{year}_exp_monnorm_{rule_year}', axis=1)

    df = df.set_index(['pop', 'county', 'cat', 'weight'])

    df = df.stack()
    df = df.reset_index()
    df = df.rename(columns={'level_4': 'year', 0: 'exposure'})
    # `level_4` will change if cols are added or removed

    df['exposure_weighted'] = df['exposure'] * df['weight']

    return df


if __name__ == '__main__':
    multisatpm_exposure_plot(2012, 2010, 2016)
