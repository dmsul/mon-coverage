from census_data import load_fips_cbsa, county_info

from econtools import state_fips_to_name, table_statrow, save_cli

from util.env import out_path
from analysis.misclass import blocks_misclass_flag


def county_name_with_metro():
    df = county_info().set_index('fips')
    cbsa = load_fips_cbsa().set_index('fips')['cbsa_name']
    df = df.join(cbsa)
    df = df[['name', 'cbsa_name']]
    df = df.rename(columns={'name': 'county_name'})
    df['state_name'] = [state_fips_to_name(int(x)) for x in df.index.str[:2]]
    df['cbsa_name'] = df['cbsa_name'].fillna('')
    return df


if __name__ == "__main__":
    year = 2014
    blocks = blocks_misclass_flag(year, 'pm25_12', 'msatna')
    df = blocks[~blocks['nonattain']].copy()

    df = df.drop(['has_mon_fips', 'fips_pop', 'has_over'], axis=1)
    df = (df
          .groupby('fips')['is_over'].max()
          .to_frame('fips_misclass'))
    cbsa = county_name_with_metro()
    df = cbsa.join(df).reset_index()
    misclass = df[df['fips_misclass'].fillna(False)]

    # Mark partial counties
    tmp = (blocks[blocks['has_nonattain']]
           .groupby(['fips', 'is_over'])
           .size()
           .groupby(level='fips')
           .size())
    partial_counties = tuple(tmp[tmp == 2].index.values.tolist())
    del tmp
    is_partial = misclass['fips'].isin(partial_counties)
    misclass.loc[is_partial, 'county_name'] += '*'

    misclass = misclass.sort_values(['state_name', 'county_name'])

    # Format to table
    table_str = ''
    stat_just = 40
    st_form = '\\textbf{{{}}}'
    for state_name, state_df in misclass.groupby('state_name'):
        idx_0 = state_df.index.values[0]
        table_str += table_statrow(
            st_form.format(state_df.loc[idx_0, 'state_name']),
            state_df.loc[idx_0, ['county_name', 'cbsa_name']],
            stat_just=stat_just)
        if len(state_df) == 1:
            continue

        iter_obj = state_df.drop(idx_0, axis=0)[['county_name', 'cbsa_name']]
        for __, x in iter_obj.iterrows():
            table_str += table_statrow('', x.tolist(), stat_just=stat_just)

    print(table_str)

    if save_cli():
        with open(out_path('list_counties.tex'), 'w') as f:
            f.write(table_str)
