import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from econtools import save_cli

from epa_airpoll import nonattainment_block_panel

from util.env import out_path
from analysis.basic_data import (merge_blocks_pop,
                                 msatna_blocks_3lag_panel)


def expand_nonattain(rule, year0, yearT, df):
    nonattain = nonattainment_block_panel(rule).loc[:, year0:yearT]
    nonattain_expanded = nonattain.reindex(index=df.index).fillna(False)

    return nonattain_expanded


if __name__ == '__main__':
    year0 = 2005
    yearT = 2017
    df = msatna_blocks_3lag_panel()
    df = df.loc[:, year0:yearT]

    df = merge_blocks_pop(df)

    nonatt_expand97 = expand_nonattain('pm25_97', year0, yearT, df)
    nonatt_expand12 = expand_nonattain('pm25_12', year0, yearT, df)
    pop = df.pop('pop')

    above12 = df >= 12
    above15 = df >= 15

    years = list(range(year0, yearT + 1))
    out_12 = pd.DataFrame(np.zeros((len(years), 2)),
                          index=years,
                          columns=[False, True])
    out_12.columns.name = 'nonattain'
    out_15 = pd.DataFrame(np.zeros((len(years), 2)),
                          index=years,
                          columns=[False, True])
    out_15.columns.name = 'nonattain'

    for y in years:
        if y >= 2015:
            this12 = (pop
                      .groupby([above12[y].values, nonatt_expand12[y].values])
                      .sum())
            out_12.loc[y, :] = this12.unstack().loc[True, :]

        this15 = (pop
                  .groupby([above15[y].values, nonatt_expand97[y].values])
                  .sum())
        tmp = this15.unstack().loc[True, :].fillna(0)
        out_15.loc[y, :] = tmp

    out_12 /= 1e6
    out_15 /= 1e6

    fig, ax = plt.subplots()
    ax.plot(out_15.index, out_15[False], '-o',
            color='navy', label='Attainment (1997)')
    ax.plot(out_15.index, out_15[True], '-^',
            color='maroon', label='Non-attainment (1997)')

    out_12_sub = out_12.loc[2015:, :]

    ax.plot(out_12_sub.index, out_12_sub[False], '--o',
            color='navy', label='Attainment (2012)')
    ax.plot(out_12_sub.index, out_12_sub[True], '--^',
            color='maroon', label='Non-attainment (2012)')

    ax.yaxis.grid(color='gray', alpha=0.5, zorder=9)
    ax.set_ybound(lower=0)

    box = ax.get_position()
    ax.set_position([box.x0,
                     box.y0 + box.height * 0.15,
                     box.width,
                     box.height * 0.85])

    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=2)

    ax.set_ylabel("Millions of People")
    ax.set_xlabel("Year")

    if save_cli():
        fig.savefig(out_path("ts_overexposed_msatna.pdf"),
                    bbox_inches='tight',
                    )
        fig.savefig(out_path("ts_overexposed_msatna.png"),
                    bbox_inches='tight',
                    dpi=400,
                    )
        plt.close()
    else:
        plt.show()
