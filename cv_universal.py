import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from econtools import legend_below

from multisatpm import msat_northamer_1year, multisat_conus_year
from epa_airpoll.clean.raw import monitors_annual_summary

from util.env import out_path
from analysis.frankensat import franken_year
from analysis.basic_data import _xy_to_int_multisat

xy_int = ['x_int', 'y_int']
xy = ['x', 'y']


def main(year: int, data: str, yaxis: bool=False, save: bool=False) -> None:
    pm = prep_sat_data(year, data)
    mon = prep_mon(year)
    mon = mon.join(pm['pm25'], how='inner', on=xy_int)

    fig, ax = plt.subplots()

    x_var = 'mean'
    y_var = 'pm25'
    coverage_label = {1: '$\leq$80 days/year', 2: '81-120',
                      3: '121-300', 4: '>300'}
    coverage_colors = {1: 'red', 2: 'y', 3: 'navy', 4: 'green'}
    coverage_marker = {1: 'o', 2: '^', 3: 's', 4: 'p'}
    mon['coverage_cat'] = -1
    mon.loc[mon['valid_day_count'] <= 80, 'coverage_cat'] = 1
    mon.loc[(mon['valid_day_count'] > 80) &
            (mon['valid_day_count'] <= 120), 'coverage_cat'] = 2
    mon.loc[(mon['valid_day_count'] > 120) &
            (mon['valid_day_count'] <= 300), 'coverage_cat'] = 3
    mon.loc[mon['valid_day_count'] > 300, 'coverage_cat'] = 4

    handles1 = []
    for samp in sorted(mon['coverage_cat'].unique()):
        plot = ax.scatter(
            mon.loc[mon['coverage_cat'] == samp, x_var],
            mon.loc[mon['coverage_cat'] == samp, y_var],
            marker=coverage_marker[samp],
            label=coverage_label[samp],
            color=coverage_colors[samp],
            alpha=.25,
        )
        handles1.append(plot)
    mon['expint'] = np.around(mon[x_var])
    binscatter = mon.groupby(['expint', 'coverage_cat'])[y_var].mean()
    binscatter = binscatter.unstack('coverage_cat')

    handles2 = []
    for samp in sorted(coverage_colors.keys()):
        plot = ax.scatter(
            binscatter.index, binscatter[samp],
            facecolor=coverage_colors[samp],
            marker=coverage_marker[samp],
            label=coverage_label[samp] + ' Mean',
            edgecolor='k')
        handles2.append(plot)

    space = np.linspace(mon[x_var].min(), mon[x_var].max(), 100)
    ax.plot(space, space, color='k')

    ax.grid(True, alpha=0.5, color='gray')
    naaqs = 12 if year > 2012 else 15
    ax.axvline(naaqs, color='k', linestyle=':', alpha=.5)
    ax.axhline(naaqs, color='k', linestyle=':', alpha=.5)

    ax.set_xlim(left=0)
    if yaxis:
        ax.set_ylim(bottom=0, top=25)

    labels = {
        'mean': 'Monitor (μg/m$^3$)',
        'pm25': 'Satellite (μg/m$^3$)'
    }
    ax.set_ylabel(labels[y_var])
    ax.set_xlabel(labels[x_var])

    # Order elements for legend
    hh = []
    for idx, plot in enumerate(handles1):
        hh.append(plot)
        hh.append(handles2[idx])

    legend_below(ax, hh, [h.get_label() for h in hh],
                 anchor=(0.5, -0.12),
                 ncol=4, fontsize='small', shrink=False)

    if save:
        fileroot = f'cv_{data}_{year}'
        if yaxis:
            fileroot += '_pad_yaxis'
        common_opts = dict(bbox_inches='tight',
                           transparent=True)
        fig.savefig(out_path(fileroot + '.pdf'),
                    **common_opts)
        ax.set_title(f'{year}')
        fig.savefig(out_path('cross-validation',
                             fileroot + '.png'),
                    dpi=400,
                    )
        plt.close()
    else:
        plt.show()


def prep_sat_data(year: int, data: str) -> pd.DataFrame:
    if data == 'msatna':
        df = msat_northamer_1year(year)
    elif data == 'multisatpm':
        df = multisat_conus_year(year)
    elif data == 'frankensat':
        df = franken_year(year)

    df.name = 'pm25'
    df = df.reset_index()

    df[xy_int] = _xy_to_int_multisat(df[xy])
    df = df.set_index(xy_int)

    return df


def prep_mon(year: int) -> pd.DataFrame:
    df = monitors_annual_summary(year)
    df = df[df['parameter_code'] == 88101]
    df = df[df['event_type'].isin(('No Events', 'Events Included'))]
    df = df[df['pollutant_standard'] == 'PM25 Annual 2006']

    df[xy_int] = _xy_to_int_multisat(df[['longitude', 'latitude']])

    df = df.rename(columns={'arithmetic_mean': 'mean'})

    assert df.shape == df.drop_duplicates('monitor_id').shape

    return df


if __name__ == '__main__':
    import argparse

    opts = argparse.ArgumentParser()
    opts.add_argument('--year', type=int)
    opts.add_argument('--all', action='store_true')
    opts.add_argument('--data', type=str,
                      choices=['multisatpm', 'msatna', 'frankensat'])
    opts.add_argument('--yaxis', action='store_true')
    opts.add_argument('--save', action='store_true')

    args = opts.parse_args()

    if args.year:
        main(args.year, args.data, yaxis=args.yaxis, save=args.save)
    elif args.all:
        for y in range(2002, 2017):
            main(y, args.data, save=args.save)
    else:
        raise ValueError("Must specify year.")
