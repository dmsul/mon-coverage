import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter

from econtools import save_cli

from epa_airpoll import nonattainment_block_panel, blocks_population

from util.env import out_path
from analysis.normed_block_exp import multisat_normed_year
from analysis.basic_data import (merge_blocks_pop, msatna_blocks_3lag_panel,
                                 block_has_monitor)


if __name__ == "__main__":
    df = blocks_population().to_frame('pop')
    df = df[df['pop'] > 0].copy()
    df = df.join(
        nonattainment_block_panel('pm25_12')[2015].to_frame('nonatt')
    )
    df['nonatt'] = df['nonatt'].fillna(False)

    df = df.join(msatna_blocks_3lag_panel()[2015].to_frame('exp'))
    df = df.join(block_has_monitor(2015).to_frame())
    df['has_mon'] = df['has_mon'].fillna(False)

    df['fips'] = df.index.str[:5]
    mon_exp = df[df['has_mon']].groupby('fips')['exp'].max()
    df = df.join(mon_exp.to_frame('mon_exp'), on='fips')

    df['normed_exp'] = df['exp'] / df['mon_exp']

    plot_df = df[df['mon_exp'].notnull()]

    # Define bins
    plot_var = 'normed_exp'
    step = 0.1
    samp_min = plot_df[plot_var].min()     # Not `plot_df` so shared axes
    samp_max = plot_df[plot_var].max()
    bin0 = 1.15
    while bin0 > samp_min:
        bin0 -= step
    bins = np.arange(bin0, samp_max + step, step)

    plot_df['bins'] = pd.cut(plot_df[plot_var], bins, labels=bins[1:]-.125)
    hist = plot_df.groupby('bins')['pop'].sum()

    fig, ax = plt.subplots()
    ax.bar(bins[:-1], hist.values / hist.sum(), step, align='edge',
           color='navy')

    ticks = np.arange(bin0 + step / 2, samp_max + step / 2, step)
    ax.set_xticks(ticks)
    ax.set_xticklabels(ticks, rotation=50)
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))

    plt.show()
