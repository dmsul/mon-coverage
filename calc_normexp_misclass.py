import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from econtools import save_cli

from util.env import out_path
from analysis.misclass import blocks_misclass_flag


def main(save: bool=False) -> None:
    df = prep_data()

    calc_frac_without_mons(df, save=save)
    plot_normexp_distribution(df, save=save)


def calc_frac_without_mons(df0: pd.DataFrame, save: bool=False) -> None:
    df = df0[df0['misclassed_fips']]
    frac_wo_mon = df.loc[~df['has_mon_fips'], 'pop'].sum() / df['pop'].sum()
    out_str = f'Frac pop w/o mon:\t{frac_wo_mon}\n'

    df2 = df[['fips', 'has_mon_fips']].drop_duplicates()
    frac_fips_wo_mon = 1 - df2['has_mon_fips'].mean()
    out_str += f'Frac fips w/o mon:\t{frac_fips_wo_mon}'
    print(out_str)

    if save:
        with open(out_path("calc_frac_without_mons.txt"), 'w') as f:
            f.write(out_str)


def plot_normexp_distribution(df0: pd.DataFrame, save: bool=False) -> None:
    df = df0[df0['misclassed_fips']]
    df = df[df['normed_exp'].notnull()]

    # Define bins
    step = 0.1
    samp_min = df['normed_exp'].min()
    samp_max = df['normed_exp'].max()
    bin0 = 1.1
    while bin0 > samp_min:
        bin0 -= step
    bins = np.arange(bin0, samp_max + step, step)

    frac_over = df.loc[df['normed_exp'] > 1, 'pop'].sum() / df['pop'].sum()
    out_str = f"Frac of pop over 1:\t{frac_over}\n"
    print(out_str)
    if save:
        with open(out_path("calc_frac_normexp_over1.txt"), 'w') as f:
            f.write(out_str)

    df['bins'] = pd.cut(df['normed_exp'], bins, labels=bins[1:]-.1)
    hist = df.groupby('bins')['pop'].sum()

    fig, ax = plt.subplots()
    ax.bar(bins[:-1], hist.values / hist.sum(), step, align='edge',
           color='navy')
    ax.set_yscale('log')

    ax.set_ylabel("Density (people)")
    ax.set_xlabel("PM$_{2.5}$ Concentration Relative to Monitor")

    if save:
        fig.savefig(out_path("hist_normexp_misclass.pdf"),
                    bbox_inches='tight',
                    )
        plt.close()
    else:
        plt.show()


def prep_data() -> pd.DataFrame:
    df = blocks_misclass_flag(2016, 'pm25_12')

    fips_mon_exp = df[df['has_mon_block']].groupby('fips')['exp'].mean()
    df = df.join(fips_mon_exp.to_frame('mons_exp'), on='fips')
    df['normed_exp'] = df['exp'] / df['mons_exp']

    return df


if __name__ == '__main__':
    main(save=save_cli())
