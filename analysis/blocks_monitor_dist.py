import numpy as np
import pandas as pd

from cyvincenty import vincenty_cross
from econtools import load_or_build

from epa_airpoll.util import fips_to_name_xwalk
from epa_airpoll.clean.raw import (sites_data, monitors_data,
                                   pollutant_crosswalk)
from epa_airpoll.clean.census.shapefiles import load_blocks_shape_info_state

from util.env import data_path


# TODO: Generalize to any pollutant. Currently hard-coded for PM2.5


@load_or_build(data_path('blocks_monitor_dist.pkl'))
def blocks_monitor_dist():
    dfs = [blocks_monitor_dist_state(state)
           for state in list(fips_to_name_xwalk.keys())]
    df = pd.concat(dfs)
    return df


@load_or_build(data_path('blocks_monitor_dist_{}.pkl'), path_args=[0])
def blocks_monitor_dist_state(state):
    state_block = load_blocks_shape_info_state(state)
    sites = prep_sites(state_block)
    latlon = ['latitude', 'longitude']
    sites[latlon] = sites[latlon].astype(np.float32)
    xy = ['x', 'y']
    state_block[xy] = state_block[xy].astype(np.float32)

    print(state_block.shape)
    print(sites.shape)

    wut = vincenty_cross(
        state_block['x'].values,
        state_block['y'].values,
        sites['longitude'].values,
        sites['latitude'].values,
    )
    dists = pd.DataFrame(wut,
                         columns=sites.index,
                         index=state_block.index)
    dists = dists.replace(-1, np.nan)

    df = dists.min(axis=1).to_frame('min_dist')
    df['num_within_15km'] = (df < 15).sum(axis=1)

    return df


def prep_sites(block_df):
    TARGET_YEAR = 2006

    sites = sites_data()
    sites = sites[
        (sites['start_date'].dt.year < TARGET_YEAR) &
        (sites['end_date'].dt.year > TARGET_YEAR)
    ]
    sites = (sites[['site_id', 'latitude', 'longitude']]
             .set_index('site_id'))
    sites = sites[~sites.isnull().max(axis=1)]

    # Bound by 1 deg
    x0, x1 = block_df['x'].min(), block_df['x'].max()
    y0, y1 = block_df['y'].min(), block_df['y'].max()

    buff = 1.5
    within_1deg = (
        (sites['latitude'] >= (y0 - buff)) &
        (sites['latitude'] <= (y1 + buff)) &
        (sites['longitude'] >= (x0 - buff)) &
        (sites['longitude'] <= (x1 + buff))
    )
    sites = sites[within_1deg]

    # Restrict to pm_25 and NAAQS
    monitors = prep_monitors()
    sites = sites.join(monitors, how='inner')

    return sites


def prep_monitors():
    df = monitors_data()
    df = df[df['naaqs_primary_monitor'] == 'Y']
    df = df[df['parameter_code'] == pollutant_crosswalk['pm_25']]
    df = df[['site_id']].set_index('site_id').squeeze()
    return df


if __name__ == '__main__':
    df = blocks_monitor_dist()
