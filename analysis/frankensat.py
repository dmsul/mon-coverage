import pandas as pd

from econtools import load_or_build

from multisatpm import multisat_conus_year, msat_northamer_1year

from util.env import data_path
from analysis.basic_data import (_xy_to_int_multisat, _int_to_xy_multisat,
                                 panel_to_3lag)

xy_int = ['x_int', 'y_int']
xy = ['x', 'y']


@load_or_build(data_path('frankensat_3lag_{}.pkl'), path_args=[0])
def franken_3lag_year(year: int) -> pd.DataFrame:
    return franken_3lag_panel()[year].squeeze()


@load_or_build(data_path('frankensat_3lag_panel.pkl'))
def franken_3lag_panel() -> pd.DataFrame:
    df = franken_panel()
    df = panel_to_3lag(df)
    return df


@load_or_build(data_path('frankensat_panel.pkl'))
def franken_panel() -> pd.DataFrame:
    dfs = [franken_year(y) for y in range(2002, 2016 + 1)]
    df = pd.concat(dfs, axis=1)
    return df


@load_or_build(data_path('frankensat_{}.pkl'), path_args=[0])
def franken_year(year: int) -> pd.DataFrame:
    df1 = _prep_for_merge(multisat_conus_year(year)).to_frame('old')
    df2 = _prep_for_merge(msat_northamer_1year(year)).to_frame('new')
    df = df1.join(df2, how='inner')
    df_new = _cleanup_merge(df.mean(axis=1))
    df_new.name = year

    return df_new


def _prep_for_merge(df0: pd.DataFrame) -> pd.DataFrame:
    df = df0.reset_index()
    df[xy_int] = _xy_to_int_multisat(df[xy])
    df = (df
          .drop(xy, axis=1)
          .set_index(xy_int)
          .squeeze()
          .sort_index())
    return df


def _cleanup_merge(df0: pd.DataFrame) -> pd.DataFrame:
    df = df0.reset_index()
    df[xy] = _int_to_xy_multisat(df[xy_int])
    df = (df
          .drop(xy_int, axis=1)
          .set_index(xy)
          .squeeze())
    return df


if __name__ == "__main__":
    for y in range(2002, 2015):
        df = franken_year(y)
        del df
    df = franken_year(2016)
