from analysis.geo_exposure import (multisatpm_exposure_block_conus,
                                   _xy_to_int_multisat)
from analysis.basic_data import (multisatpm_years_block, merge_blocks_xy,
                                 merge_blocks_pop, block_has_monitor)


xy_int = ['x_int', 'y_int']
xy = ['x', 'y']


def multisat_normed_year(norm_year, exp_year):
    return multisat_normed_panel(norm_year, exp_year, exp_year)


def multisat_normed_panel(norm_year, start_year, end_year):

    df = prep_blocks_multisatpm(start_year, end_year)

    df = df.join(block_has_monitor(norm_year))

    # Add `norm_year` if needed
    if not (start_year <= norm_year <= end_year):
        norm_year_exp = multisatpm_exposure_block_conus(norm_year)
        norm_year_exp = norm_year_exp.squeeze().to_frame(norm_year)
        df = df.join(norm_year_exp)

    # Get reading in monitors' grid
    df['fips'] = df.index.str[:5]
    mons_multisat = df[df['has_mon']].groupby('fips')[norm_year].mean()
    df = df.join(mons_multisat.to_frame(f'{norm_year}_exp_at_mon'), on='fips')

    for y in range(start_year, end_year + 1):
        df[f'{y}_exp_monnorm_{norm_year}'] = (df[y] /
                                              df[f'{norm_year}_exp_at_mon'])

    return df


def prep_blocks_multisatpm(start_year, end_year):
    df = multisatpm_years_block(start_year, end_year)

    df = merge_blocks_xy(df)
    df = merge_blocks_pop(df)

    df[xy_int] = _xy_to_int_multisat(df[xy])

    return df


if __name__ == '__main__':
    pass
