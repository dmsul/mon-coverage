import argparse
import pandas as pd

import matplotlib.pyplot as plt

from econtools import legend_below

from epa_airpoll import nonattainment_block_panel

from util import pmrule_imp_year
from util.env import out_path
from analysis.basic_data import (msatna_blocks_panel, merge_blocks_pop,
                                 msatna_blocks_3lag_panel,
                                 monitors_block,
                                 blocks_multisatpm_withpop_panel,
                                 panel_to_3lag
                                 )
from analysis.monitor_sample import monitors_summary_panel


def main(rule, data, split_by_naaqs=True, save=False):
    sat_means = prep_sat_data(rule, data, split_by_naaqs=split_by_naaqs)
    mon_means = prep_mon_data(rule)

    year0 = 2012
    mon_means = mon_means.loc[year0:, :]
    sat_means = sat_means.loc[year0:, :]

    sat_means = sat_means.diff()
    mon_means = mon_means.diff()

    fig, ax = plt.subplots()

    ax.plot(mon_means.index, mon_means['nonatt,above'], '-o',
            color='maroon', label='Monitor',
            )
    ax.plot(sat_means.index, sat_means['nonatt,above'], '--^',
            color='maroon', label='Satellite',
            )

    ax.set_ylabel("Change in PM$_{2.5}$ lagged 3-year average ($\mu$g/m$^3$)")
    # ax.set_yticks(list(range(7, 17)))
    ax.set_xlabel("Year")

    ax.axvline(pmrule_imp_year[rule],
               color='g', linestyle=':', zorder=2)

    ax.yaxis.grid(color='gray', alpha=0.5, zorder=1)

    legend_below(ax, ncol=2)

    if save:
        filepath = out_path(f'plot_ts_joint_{rule}.pdf')
        fig.savefig(filepath, bbox_inches='tight')
        fig.savefig(filepath.replace('.pdf', '.png'),
                    bbox_inches='tight',
                    dpi=400)
        plt.close()
    else:
        plt.show()


def prep_mon_data(rule: str) -> pd.DataFrame:
    df = _pm_nonattainment_data(rule)

    imp_year_val = df.loc[df['year'] == pmrule_imp_year[rule],
                          ['monitor_id', 'mean']]
    imp_year_val = (imp_year_val
                    .set_index('monitor_id')
                    .squeeze())
    naaqs = 15 if rule == 'pm25_97' else 12
    impyear_over = imp_year_val >= naaqs
    df = df.join(impyear_over.to_frame('impyear_over'),
                 on='monitor_id')
    df['impyear_over'] = df['impyear_over'].astype(bool)

    df['cat'] = ''
    flag = (df['nonattain']) & (df['impyear_over'])
    df.loc[flag, 'cat'] = 'nonatt,above'
    noflag = (df['nonattain']) & (~df['impyear_over'])
    df.loc[noflag, 'cat'] = 'nonatt,below'
    df.loc[~df['nonattain'], 'cat'] = 'att'
    means = df.groupby(['cat', 'year'])['mean'].mean().unstack('cat')

    return means


def _pm_nonattainment_data(rule, lag3=True):
    df = monitors_summary_panel()

    # Determine monitor start
    start_year = df.groupby('monitor_id')['year'].min()
    end_year = df.groupby('monitor_id')['year'].max()
    df = df.join(start_year.to_frame('start_year'), on='monitor_id')
    df = df.join(end_year.to_frame('end_year'), on='monitor_id')

    # restrict to monitors 3 years before rule and 3 years after implementation
    imp_year = pmrule_imp_year[rule]
    if rule == 'pm25_97':
        year0 = 2000
        yearT = 2010
    elif rule == 'pm25_12':
        year0 = 2009
        yearT = df['end_year'].max()
    good_mons = ((df['start_year'] <= imp_year - 2) &
                 (df['end_year'] >= imp_year + 2))
    df = df[good_mons]
    good_years = (df['year'] >= year0) & (df['year'] <= yearT)
    df = df[good_years]

    if lag3:
        df = df.set_index(
            ['monitor_id', 'start_year', 'end_year', 'fips', 'year']
        )
        df = df['arithmetic_mean']
        df = df.unstack('year')
        df = panel_to_3lag(df)
        df.columns.name = 'year'
        df = df.stack('year')
        df.name = 'arithmetic_mean'
        df = df.reset_index()

    df = df.set_index('fips')

    nonattain = _prep_monitors_nonattain(rule)
    df = df.join(nonattain, on='monitor_id', how='left')
    df['nonattain'] = df['nonattain'].fillna(False)

    df = df.rename(columns={'arithmetic_mean': 'mean'})

    return df

def _prep_monitors_nonattain(rule):
    nonattain = nonattainment_block_panel(rule)[pmrule_imp_year[rule]]
    nonattain = nonattain.to_frame('nonattain')

    df = (monitors_block()
          .to_frame('block_id')
          .join(nonattain, on='block_id', how='left'))
    df['nonattain'] = df['nonattain'].fillna(False)
    df = df.drop('block_id', axis=1)

    return df


def prep_sat_data(rule, data, split_by_naaqs=False):
    if data == 'msatna':
        df = msatna_blocks_panel()
    if data == 'msatna3':
        df = msatna_blocks_3lag_panel()
    elif data == 'multisatpm':
        df = blocks_multisatpm_withpop_panel()
    elif data == 'multisatpm3':
        df = blocks_multisatpm_withpop_panel()
        df = panel_to_3lag(df)

    # Merge nonattainment, block-level
    imp_year = pmrule_imp_year[rule]
    nonatt = (nonattainment_block_panel(rule)[imp_year]
              .to_frame('nonatt'))
    df = df.join(nonatt)
    df['nonatt'] = df['nonatt'].fillna(False)

    # Get pop
    df = merge_blocks_pop(df)

    # Over in imp year
    if split_by_naaqs:
        imp_year_exp = df[imp_year]
        naaqs = 12 if rule == 'pm25_12' else 15
        df['cat'] = 'att'
        df.loc[(df['nonatt']) & (imp_year_exp < naaqs),
               'cat'] = 'nonatt,below'
        df.loc[(df['nonatt']) & (imp_year_exp >= naaqs),
               'cat'] = 'nonatt,above'
    else:
        df['cat'] = df['nonatt']

    # Group pop
    group_pop = df.groupby(df['cat'])['pop'].sum()
    df = df.join(group_pop.to_frame('group_pop'), on='cat')

    weight = (df['pop'] / df['group_pop']).squeeze()
    exp = df.drop(['pop', 'group_pop', 'nonatt'], axis=1)
    cat = exp.pop('cat')
    exp_wt = exp.multiply(weight, axis=0)

    means = exp_wt.groupby(cat).sum().T
    means.index = means.index.astype(int)

    return means


def cli():
    opts = argparse.ArgumentParser()
    opts.add_argument('--save', action='store_true')

    args = opts.parse_args()

    return args


if __name__ == "__main__":
    args = cli()
    rule = 'pm25_12'
    data = 'msatna3'
    main(rule, data, save=args.save)
