import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.io.shapereader import Reader
from cartopy.mpl.patch import geos_to_path
from matplotlib.path import Path
import matplotlib as mpl
from matplotlib.lines import Line2D
from matplotlib.colors import Normalize

from modis.clean.raw import load_modis_year
from epa_airpoll import monitors_data, nonattainment_shape
from multisatpm import (multisat_conus_year, msat_northamer_1year)

from util import conus_bounds
from util.env import gis_src_path, src_path, out_path
from analysis.basic_data import msatna_3lag_year, multisatpm_3lag_year
from analysis.monitor_sample import (valid_naaqs_monitors,
                                     monitors_summary_panel)


def main_conus(year, data='modis', transparent=False, save=False, **kwargs):

    fig, ax, norm = map_year(year, data=data, area='conus')

    if 1:
        shp_path = gis_src_path('census', 'cb_2016_us_state_5m',
                                'cb_2016_us_state_5m.shp')
        adm_shapes = list(Reader(shp_path).geometries())
        ax.add_geometries(adm_shapes, ccrs.PlateCarree(),
                          edgecolor='0.9', facecolor='none', linewidth=0.1,
                          alpha=.4,
                          zorder=9)

    if save:
        fig.savefig(out_path(f'map_{data}_conus_{year}.png'),
                    dpi=600,
                    bbox_inches='tight',
                    transparent=transparent)
        plt.close()
    else:
        plt.show()


def main_hotspot_OH(year, data='modis', transparent=False, rule='pm25_97',
                    save=False):

    fig, ax, norm = map_year(year, data, area='hotspotOH')

    county_line_color = 'white'
    nonattain_line_color = '#FF8933'
    ax = draw_county_attainment(ax, rule, year, attain_edge=county_line_color,
                                nonattain_edge=nonattain_line_color)
    ax = draw_monitor_locations(ax, year, facecolor='black', s=1.2, alpha=1,
                                edgecolor='white',
                                lw=.2,
                                marker='o', zorder=5,
                                label='Monitors')
    handles, labels = ax.get_legend_handles_labels()

    leg = ax.legend(handles=[
        Line2D([0], [0],
               color=nonattain_line_color,
               lw=.5,
               label='Nonattainment area'),
        Line2D([0], [0],
               color=county_line_color,
               lw=.5,
               label='County borders')] + handles,
        loc='lower right',
        fontsize='x-small',
        framealpha=.9,
    )
    leg.get_frame().set_facecolor('0.8')

    city_labels = (
        # City, x, y, text color, background color
        ('Chicago', -87.61, 41.92, 'black', 'none'),
        ('Cleveland', -82.32, 41.65, 'black', 'none'),
        ('Indianapolis', -86.03, 39.73, 'white', '0.5'),
        ('Pittsburgh', -80.29, 39.87, 'white', '0.5'),
        ('Louisville', -85.50, 38.20, 'white', '0.5'),
        ('Cincinnati', -84.16, 39.30, 'white', '0.5'),
    )
    for city_name, x, y, color, bg in city_labels:
        ax.text(x, y, city_name,
                color=color,
                bbox=dict(facecolor=bg, alpha=0.5, edgecolor='none', pad=1),
                fontsize='xx-small',
                )

    if save:
        fig.savefig(out_path(f'map_{data}_hotspotOH_{rule}_{year}.png'),
                    dpi=500,
                    bbox_inches='tight',
                    transparent=transparent
                    )
        plt.close()
    else:
        plt.show()


def main_hotspot_CA(year, data='modis', transparent=False, rule='pm25_97',
                    save=False):

    area = 'hotspotCA'
    fig, ax, norm = map_year(year, data, area=area)

    county_line_color = 'white'
    nonattain_line_color = '#FF8933'
    ax = draw_county_attainment(ax, rule, year, attain_edge=county_line_color,
                                nonattain_edge=nonattain_line_color)
    ax = draw_monitor_locations(ax, year, facecolor='black', s=1.2, alpha=1,
                                edgecolor='white',
                                lw=.2,
                                marker='o', zorder=5,
                                label='Monitors')
    handles, labels = ax.get_legend_handles_labels()

    leg = ax.legend(handles=[
        Line2D([0], [0],
               color=nonattain_line_color,
               lw=.5,
               label='Nonattainment area'),
        Line2D([0], [0],
               color=county_line_color,
               lw=.5,
               label='County borders')] + handles,
        loc='lower left',
        fontsize='x-small',
        framealpha=.9,
    )
    leg.get_frame().set_facecolor('0.8')

    city_labels = (
        # City, x, y, text color, background color
        ('San Diego', -117.8, 32.7, 'black', 'none'),
        ('Los Angeles', -118.98, 33.84, 'black', 'none'),
        ('Ventura', -119.28, 34.45, 'white', '0.7'),
        ('Yuma', -114.7, 32.8, 'white', '0.2'),
        # ('Indianapolis', -86.03, 39.73, 'white', '0.5'),
        # ('Pittsburgh', -80.29, 39.87, 'white', '0.5'),
        # ('Louisville', -85.50, 38.20, 'white', '0.5'),
        # ('Cincinnati', -84.16, 39.30, 'white', '0.5'),
    )
    for city_name, x, y, color, bg in city_labels:
        ax.text(x, y, city_name,
                color=color,
                bbox=dict(facecolor=bg, alpha=0.5, edgecolor='none', pad=1),
                fontsize='xx-small',
                )

    if save:
        fig.savefig(out_path(f'map_{data}_{area}_{rule}_{year}.png'),
                    dpi=500,
                    bbox_inches='tight',
                    transparent=transparent,
                    )
        plt.close()
    else:
        plt.show()


def main_hotspot_TX(year, data='modis', transparent=False, rule='pm25_97',
                    save=False):

    area = 'hotspotTX'
    fig, ax, norm = map_year(year, data, area=area)

    county_line_color = 'white'
    nonattain_line_color = '#FF8933'
    ax = draw_county_attainment(ax, rule, year, attain_edge=county_line_color,
                                nonattain_edge=nonattain_line_color)
    ax = draw_monitor_locations(ax, year, facecolor='black', s=1.2, alpha=1,
                                edgecolor='white',
                                lw=.2,
                                marker='o', zorder=5,
                                label='Monitors')
    handles, labels = ax.get_legend_handles_labels()

    leg = ax.legend(handles=[
        Line2D([0], [0],
               color=nonattain_line_color,
               lw=.5,
               label='Nonattainment area'),
        Line2D([0], [0],
               color=county_line_color,
               lw=.5,
               label='County borders')] + handles,
        loc='lower right',
        fontsize='x-small',
        framealpha=.9,
    )
    leg.get_frame().set_facecolor('0.8')

    city_labels = (
        # City, x, y, text color, background color
        ('Houston', -95.1, 29.9, 'white', '0.6'),
        ('Dallas', -96.7, 32.8, 'white', '0.6'),
        ('San Antonio', -98.3, 29.42, 'white', '0.6'),
        ('Austin', -97.5, 30.27, 'white', '0.6'),
        ('Del Rio', -101.5, 29.2, 'black', 'none'),
        ('Eagle Pass', -101.45, 28.6, 'black', 'none'),
        # ('Laredo', -98.75, 27.5, 'white', '0.5'),
    )
    for city_name, x, y, color, bg in city_labels:
        ax.text(x, y, city_name,
                color=color,
                bbox=dict(facecolor=bg, alpha=0.5, edgecolor='none', pad=1),
                fontsize='xx-small',
                )

    if save:
        fig.savefig(out_path(f'map_{data}_{area}_{rule}_{year}.png'),
                    dpi=500,
                    bbox_inches='tight',
                    transparent=transparent
                    )
        plt.close()
    else:
        plt.show()


def main_cali(year, data='diff', save=False, **kwargs):

    fig, ax, norm = map_year(year, data, area='cali', osm=False)

    df = monitors_data()
    df = df[df['parameter_code'] == 88101]
    df = df[(df['start_date'].dt.year <= year) &
            (df['end_date'].dt.year >= year)]
    df = df[df['state_code'] == 6]
    df = df[df['naaqs_primary_monitor'] == 'Y']

    exp = monitors_summary_panel()
    exp = exp[exp['year'] == year]
    exp = exp.set_index('monitor_id')
    is_over = exp['arithmetic_mean'] >= 12

    df = df.join(is_over.to_frame('is_over'), on='monitor_id', how='inner')

    scatter_kwargs = {
        True: {
            'marker': '*',
            'color': 'orange',
            'label': 'Monitor, over NAAQS',
            's': 10,
            'zorder': 9,
        },
        False: {
            'marker': '*',
            'color': 'green',
            'label': 'Monitor, under NAAQS',
            's': 10,
            'zorder': 9,
        }
    }

    for flag in (True, False):
        this_df = df[df['is_over'] == flag]
        ax.scatter(this_df['longitude'], this_df['latitude'],
                   **scatter_kwargs[flag])

    leg = ax.legend(loc='lower left')       # noqa

    if save:
        fig.savefig(out_path(f'map_{data}_cali_{year}.png'),
                    dpi=500,
                    bbox_inches='tight')
        plt.close()
    else:
        plt.show()


def main_pa(year, data='modis', rule='pm25_97', save=False):

    fig, ax, norm = map_year(year, data, area='pa')

    ax = draw_county_attainment(ax, rule, year,
                                attain_edge='black',
                                states=['42'])

    if save:
        if data == 'modis':
            fig.savefig(out_path(f'map_aod_pa_{rule}_{year}.png'),
                        dpi=600,
                        bbox_inches='tight')
            plt.close()
        elif data == 'multisatpm':
            fig.savefig(out_path(f'map_multisatpm_pa_{rule}_{year}.png'),
                        dpi=600,
                        bbox_inches='tight')
            plt.close()
        else:
            raise ValueError(f"data {data} is invalid")
    else:
        plt.show()


def draw_monitor_locations(ax, year, naaqs=True, **kwargs):
    df = valid_naaqs_monitors(year)

    ax.scatter(df['longitude'], df['latitude'], **kwargs)

    return ax


def draw_county_attainment(ax, rule, year,
                           attain_edge='white', nonattain_edge='black',
                           states=[]):
    """
    Draw county lines on map in `ax` with (optional) different color borders by
    attainment status.

    `states` can be a list of states (FIPS in string) to restrict drawing to.
    """
    shp_path = src_path('county_shapefiles',
                        'gz_2010_us_050_00_500k.shp')

    df = gpd.read_file(shp_path)

    crs = ccrs.PlateCarree()
    crs_proj4 = crs.proj4_init
    df = df.to_crs(crs_proj4)

    if states:
        df = df[df['STATE'].isin(states)].copy()

    ax.add_geometries(df['geometry'],
                      color='None', edgecolor=attain_edge,
                      linewidth=.1, crs=crs,
                      )

    if rule == 'pm25_12':
        actual_year = max(2015, year)
    elif rule == 'pm25_12':
        actual_year = max(2005, year)
    nonatt_shp = nonattainment_shape(actual_year)

    ax.add_geometries(nonatt_shp['geometry'],
                      color='None', edgecolor=nonattain_edge,
                      linewidth=.5, crs=crs,
                      )

    return ax


def map_year(year, data='modis', area='conus', norm=None, osm=False, ticks=[]):

    naaqs = 15 if year <= 2012 else 12

    if data == 'modis':
        df = prep_modis(year)
    elif data == 'multisatpm':
        df = multisat_conus_year(year).reset_index()
        df = df.rename(columns={0: 'pm25'})
    elif data == 'multisatpm3':
        df = multisatpm_3lag_year(year)
        df.name = 'pm25'
        df = df.reset_index()
    elif data == 'msatna':
        df = msat_northamer_1year(year).reset_index()
    elif data == 'msatna3':
        df = msatna_3lag_year(year).reset_index()
    elif data == 'diff':
        df0 = msat_northamer_1year(year - 1)
        dfT = msat_northamer_1year(year + 1)
        df = (dfT - df0).squeeze().to_frame('diff').reset_index()
    else:
        raise ValueError(f"data {data} is invalid")

    if area == 'conus':
        boundary_shape = get_conus_shape()
        x0, x1, y0, y1 = conus_bounds
    elif area == 'pa':
        boundary_shape = get_pa_shape()
        x0, y0, x1, y1 = boundary_shape.bounds
    elif area == 'hotspotOH':
        boundary_shape = get_conus_shape()
        x0 = -88.5
        x1 = -79.3
        y0 = 37.5
        y1 = 42.25
    elif area == 'hotspotCA':
        boundary_shape = get_conus_shape()
        x0 = -119.5
        x1 = -113.85
        y0 = 32.5
        y1 = 34.9
    elif area == 'hotspotTX':
        boundary_shape = get_conus_shape()
        x0 = -102.0
        x1 = -93.5
        y0 = 27.0
        y1 = 33
    elif area == 'cali':
        boundary_shape = get_conus_shape()
        # x0, x1, y0, y1 = -119, -118.3, 34., 34.6
        # x0, x1, y0, y1 = -119.5, -117.3, 33., 35
        x0, x1, y0, y1 = -118.5, -117.3, 33., 34
    else:
        raise ValueError(f"area {area} is invalid")

    df = df[
        (df['x'] > x0) & (df['x'] < x1) &
        (df['y'] > y0) & (df['y'] < y1)
    ].copy()

    # grid cells
    xy = ['x', 'y']

    if data == 'modis':
        df[xy] = np.around(np.around(df[xy] * 100) / 5) * 5 / 100
        df2 = df.groupby(xy)['aod'].mean()
        df3 = df2.unstack('x')
    else:
        df = df.set_index(xy).squeeze()
        df3 = df.unstack('x')

    if data != 'diff':
        df3 -= naaqs

    pollutant = np.ma.masked_array(df3.values, np.isnan(df3.values))
    x, y = np.meshgrid(df3.columns, df3.index)

    samp_min = int(np.floor(pollutant.min()))
    samp_max = int(np.ceil(pollutant.max()))

    if osm:
        from cartopy.io.img_tiles import OSM
        osm_tiles = OSM()
        ax = plt.axes(projection=osm_tiles.crs)
    else:
        ax = plt.axes(projection=ccrs.PlateCarree())

    # colorbar tick marks

    if ticks:
        pass
    elif data == 'modis':
        if area == 'conus':
            ticks = [8, 15, 20, 25, 30, 35, 45]
        elif area == 'pa':
            ticks = [samp_min, 10, 12, 15, 20, 30, samp_max]
        elif 'hotspot' in area:
            ticks = [samp_min, 15, 20, 25, 30, 35, samp_max]
        else:
            raise ValueError(f"data {data} is invalid")
    else:
        pass

    non_pa_cmap = 'seismic'
    cmap = non_pa_cmap
    midpoint_norm_center = 0

    # figure colors
    if norm is not None:
        pass

    if data == 'diff':
        norm = MidpointNormalize(midpoint=midpoint_norm_center,
                                 vmin=samp_min,
                                 vmax=samp_max)
    elif area == 'conus':
        if data == 'modis':
            norm = mpl.colors.LogNorm(vmin=8, vmax=45)
        else:
            norm = MidpointNormalize(
                midpoint=midpoint_norm_center,
                vmin=samp_min,
                vmax=samp_max)
    elif area == 'pa':
        norm = mpl.colors.BoundaryNorm(boundaries=np.array(ticks), ncolors=300)
        cmap = 'RdYlGn_r'
    elif 'hotspot' in area:
        if 0:
            norm = mpl.colors.Normalize(vmin=samp_min, vmax=samp_max)
        else:
            naaqs = 15 if year <= 2012 else 12
            norm = MidpointNormalize(
                midpoint=midpoint_norm_center,
                vmin=samp_min,
                vmax=samp_max)

    if osm:
        zoom_level = 9
        ax.add_image(osm_tiles, zoom_level, zorder=1)

    plot = ax.pcolormesh(
        x, y, pollutant, transform=ccrs.PlateCarree(),
        cmap=cmap,
        norm=norm,
        # zorder=2,
        # alpha=.7,
    )

    trans = ccrs.PlateCarree()._as_mpl_transform(ax)

    # restrict exposure colors to `boundary_shape`
    plot.set_clip_path(
        Path.make_compound_path(*geos_to_path(boundary_shape)),
        transform=trans
    )
    if area == 'conus':
        ax.outline_patch.set_visible(False)
        ax.background_patch.set_visible(False)

    # colorbar legend
    fig = plt.gcf()
    cb = fig.colorbar(plot, pad=0.05, orientation='horizontal',
                      # fraction=.05,
                      aspect=40,
                      shrink=.8,
                      )
    cb.ax.tick_params(labelsize=8)

    # map label
    if data == 'modis':
        cb.set_label(r'AOD scaled to $\mu$g/m$^3$ PM$_{2.5}$', fontsize=8)
    elif data == 'msatna':
        cb.set_label(r'PM$_{2.5}$ (μg/m$^3$)', fontsize=8)
    elif data == 'msatna3':
        fontsize = 8
        if area == 'conus':
            fontsize = 8
            cb.ax.tick_params(labelsize=6)
        cb.set_label(
            r'Difference between Local PM$_{2.5}$ Design Value '
            'and National Standard (μg/m$^3$)',
            fontsize=fontsize)
    elif data == 'diff':
        cb.set_label(r'Change in PM$_{2.5}$ (μg/m$^3$)', fontsize=8)

    # If map is contained shape (CONUS, one state) then no border
    if 'hotspot' not in area:
        ax.outline_patch.set_edgecolor('none')

    ax.set_extent([x0, x1, y0, y1], ccrs.PlateCarree())

    return fig, ax, norm


def prep_modis(year):
    df = load_modis_year(year).reset_index()
    df['aod'] = df['aod'] * 29.4 + 8.8
    return df


def get_pa_shape():
    shp_path = gis_src_path(
        'census', 'cb_2016_us_state_5m', 'cb_2016_us_state_5m.shp')
    df = gpd.read_file(shp_path)
    return df.loc[df['NAME'] == 'Pennsylvania', 'geometry'].squeeze()


def get_conus_shape():
    us_shape = list(Reader(src_path(
        'cb_2016_us_nation_5m', 'cb_2016_us_nation_5m.shp'
    )).records())[0].geometry
    return us_shape


class MidPointNorm(Normalize):
    """
    From
    https://stackoverflow.com/questions/7404116/defining-the-midpoint-of-a-colormap-in-matplotlib
    """
    def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
        Normalize.__init__(self, vmin, vmax, clip)
        self.midpoint = midpoint

    def __call__(self, value, clip=None):
        if clip is None:
            clip = self.clip

        result, is_scalar = self.process_value(value)

        self.autoscale_None(result)
        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

        if not (vmin < midpoint < vmax):
            raise ValueError("midpoint must be between maxvalue and minvalue.")
        elif vmin == vmax:
            result.fill(0)  # Or should it be all masked? Or 0.5?
        elif vmin > vmax:
            raise ValueError("maxvalue must be bigger than minvalue")
        else:
            vmin = float(vmin)
            vmax = float(vmax)
            if clip:
                mask = np.ma.getmask(result)
                result = np.ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                     mask=mask)

            # ma division is very slow; we can take a shortcut
            resdat = result.data

            # First scale to -1 to 1 range, than to from 0 to 1.
            resdat -= midpoint
            resdat[resdat > 0] /= abs(vmax - midpoint)
            resdat[resdat < 0] /= abs(vmin - midpoint)

            resdat /= 2.
            resdat += 0.5
            result = np.ma.array(resdat, mask=result.mask, copy=False)

        if is_scalar:
            result = result[0]
        return result

    def inverse(self, value):
        if not self.scaled():
            raise ValueError("Not invertible until scaled")
        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

        if mpl.cbook.iterable(value):
            val = np.ma.asarray(value)
            val = 2 * (val-0.5)
            val[val > 0] *= abs(vmax - midpoint)
            val[val < 0] *= abs(vmin - midpoint)
            val += midpoint
            return val
        else:
            val = 2 * (val - 0.5)
            if val < 0:
                return val*abs(vmin-midpoint) + midpoint
            else:
                return val*abs(vmax-midpoint) + midpoint


class MidpointNormalize(Normalize):
    """
    From
    https://stackoverflow.com/questions/20144529/shifted-colorbar-matplotlib
    """
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))


if __name__ == '__main__':
    import argparse

    opts = argparse.ArgumentParser()
    opts.add_argument('area', type=str)
    opts.add_argument('--year', type=int, default=2014)
    opts.add_argument('--data', type=str, default='msatna3',
                      choices=['modis', 'multisatpm', 'multisatpm3', 'msatna3',
                               'msatna', 'diff'])
    opts.add_argument('--rule', type=str, default='pm25_12')
    opts.add_argument('--transparent', action='store_true')
    opts.add_argument('--save', action='store_true')

    args = opts.parse_args()

    if args.area == 'conus':
        main_conus(args.year, data=args.data, save=args.save)
    elif args.area == 'hotspotOH':
        main_hotspot_OH(args.year, data=args.data, rule=args.rule,
                        save=args.save)
    elif args.area == 'hotspotCA':
        main_hotspot_CA(args.year, data=args.data, rule=args.rule,
                        save=args.save)
    elif args.area == 'hotspotTX':
        main_hotspot_TX(args.year, data=args.data, rule=args.rule,
                        save=args.save)
    elif args.area == 'pa':
        main_pa(args.year, data=args.data, rule=args.rule, save=args.save)
    elif args.area == 'cali':
        main_cali(args.year, data=args.data, rule=args.rule, save=args.save)
    else:
        raise ValueError(f"Area var `{args.area}` no good.")
