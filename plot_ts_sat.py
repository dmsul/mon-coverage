import argparse

import matplotlib.pyplot as plt

from econtools import legend_below

from epa_airpoll import nonattainment_block_panel

from util import pmrule_imp_year
from util.env import out_path
from analysis.basic_data import (msatna_blocks_panel, merge_blocks_pop,
                                 msatna_blocks_3lag_panel,
                                 blocks_multisatpm_withpop_panel,
                                 panel_to_3lag
                                 )


def main(rule, data, split_by_naaqs=False, save=False):
    means = prep_data(rule, data, split_by_naaqs=split_by_naaqs)

    fig, ax = plt.subplots()

    style = {
        'att':
            {'linestyle': '-',
             'marker': 'o',
             'color': 'navy',
             'label': 'Attainment',
             'zorder': 9},
        'nonatt,below':
            {'linestyle': '-',
             'marker': 'o',
             'color': 'orange',
             'label': 'Non-attainment, under NAAQS',
             'zorder': 9},
        'nonatt,above':
            {'linestyle': '-',
             'marker': 'o',
             'color': 'maroon',
             'label': 'Non-attainment, over NAAQS',
             'zorder': 9},
        False:
            {'linestyle': '-',
             'marker': 'o',
             'color': 'navy',
             'label': 'Attainment',
             'zorder': 9},
        True:
            {'linestyle': '-',
             'marker': 'o',
             'color': 'maroon',
             'label': 'Non-attainment',
             'zorder': 9},
    }

    plot_obs = {}
    for col in sorted(means.columns):
        style_dict = style[col]
        plot_obs[col] = ax.plot(means.index, means[col],
                                **style_dict)

    ax.axvline(pmrule_imp_year[rule],
               color='g', linestyle=':', zorder=2)

    ax.yaxis.grid(color='gray', alpha=0.5, zorder=1)
    ax.set_ylabel("Mean PM$_{2.5}$ ($\mu$g/m$^3$)")

    # To get legend elements in right order
    ordered = ('nonatt,above', 'nonatt,below', 'att')
    hh = [plot_obs[name][0] for name in ordered]
    legend_below(ax, hh, [h.get_label() for h in hh],
                 shrink=True, ncol=2)

    if save:
        split_infix = '_split' if split_by_naaqs else ''
        filepath = out_path(f'plot_ts_exposure_{data}_{rule}{split_infix}.pdf')
        fig.savefig(filepath, bbox_inches='tight')
        fig.savefig(filepath.replace('.pdf', '.png'),
                    bbox_inches='tight',
                    dpi=400)
        plt.close()
    else:
        plt.show()


def prep_data(rule, data, split_by_naaqs=False):
    if data == 'msatna':
        df = msatna_blocks_panel()
    if data == 'msatna3':
        df = msatna_blocks_3lag_panel()
    elif data == 'multisatpm':
        df = blocks_multisatpm_withpop_panel()
    elif data == 'multisatpm3':
        df = blocks_multisatpm_withpop_panel()
        df = panel_to_3lag(df)

    # Merge nonattainment, block-level
    imp_year = pmrule_imp_year[rule]
    nonatt = (nonattainment_block_panel(rule)[imp_year]
              .to_frame('nonatt'))
    df = df.join(nonatt)
    df['nonatt'] = df['nonatt'].fillna(False)

    # Get pop
    df = merge_blocks_pop(df)

    # Over in imp year
    if split_by_naaqs:
        imp_year_exp = df[imp_year]
        naaqs = 12 if rule == 'pm25_12' else 15
        df['cat'] = 'att'
        df.loc[(df['nonatt']) & (imp_year_exp < naaqs),
               'cat'] = 'nonatt,below'
        df.loc[(df['nonatt']) & (imp_year_exp >= naaqs),
               'cat'] = 'nonatt,above'
    else:
        df['cat'] = df['nonatt']

    # Group pop
    group_pop = df.groupby(df['cat'])['pop'].sum()
    df = df.join(group_pop.to_frame('group_pop'), on='cat')

    weight = (df['pop'] / df['group_pop']).squeeze()
    exp = df.drop(['pop', 'group_pop', 'nonatt'], axis=1)
    cat = exp.pop('cat')
    exp_wt = exp.multiply(weight, axis=0)

    means = exp_wt.groupby(cat).sum().T
    means.index = means.index.astype(int)

    if rule == 'pm25_12':
        means = means.loc[2009:, :]

    return means


def cli():
    opts = argparse.ArgumentParser()
    opts.add_argument('--rule', type=str, default='pm25_12',
                      choices=['pm25_12', 'pm25_97'])
    opts.add_argument('--data', type=str, default='msatna',
                      choices=['msatna3', 'msatna',
                               'multisatpm', 'multisatpm3'])
    opts.add_argument('--split', action='store_true')
    opts.add_argument('--save', action='store_true')

    args = opts.parse_args()

    return args


if __name__ == "__main__":
    args = cli()
    main(args.rule, args.data, split_by_naaqs=args.split, save=args.save)
