import pandas as pd
import geopandas as gpd
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from matplotlib.patches import Patch

from epa_airpoll import nonattainment_status, nonattainment_shape

from util.env import src_path, out_path

shp_path = src_path('county_shapefiles',
                    'gz_2010_us_050_00_500k.shp')


def map_attainment_county(year, rule, save=False):
    counties = gpd.read_file(shp_path)
    counties = counties[~counties['STATE'].isin(('02', '15', '72'))]
    counties.index = (counties['STATE'].astype(str).str.zfill(2) +
                      counties['COUNTY'].astype(str).str.zfill(3))
    df = nonattainment_status()
    df = df.loc[:, pd.IndexSlice[rule, year]].to_frame('nonattain')
    df = counties.join(df, how='left').fillna(False)
    crs = ccrs.PlateCarree()
    crs_proj4 = crs.proj4_init
    df = df.to_crs(crs_proj4)
    ax = plt.axes(projection=ccrs.PlateCarree())

    # Plot counties
    attainment_color = '0.9'
    ax.add_geometries(df.loc[~df['nonattain'], 'geometry'],
                      color=attainment_color, edgecolor='white', linewidth=0.1,
                      crs=crs)
    ax.add_geometries(df.loc[df['nonattain'], 'geometry'],
                      color=attainment_color, edgecolor='white', linewidth=0.1,
                      crs=crs)

    # Plot subcounty shapes
    subcount = nonattainment_shape(year)
    nonatt_color = 'k'
    ax.add_geometries(subcount['geometry'],
                      color=nonatt_color, linewidth=0.05,
                      crs=crs)

    # Re-draw county lines over non-att areas
    ax.add_geometries(df.loc[df['nonattain'], 'geometry'],
                      color='none', edgecolor='white', linewidth=0.1,
                      crs=crs)

    b = df.total_bounds
    bounds = [b[0], b[2], b[3], b[1]]
    ax.set_extent(bounds, ccrs.PlateCarree())

    ax.outline_patch.set_edgecolor('none')

    # Legend
    legend_elements = [
        Patch(color=nonatt_color,
              label='Non-attainment'),
    ]
    if 0:
        ax.legend(handles=legend_elements, loc='lower left',
                  fontsize=6)

    if save:
        plt.savefig(out_path(f'map_attainment_county_{rule}_{year}.png'),
                    dpi=500,
                    bbox_inches='tight')
        plt.close()
    else:
        plt.show()


if __name__ == '__main__':
    import argparse

    opts = argparse.ArgumentParser()
    opts.add_argument('--year', type=int, default=2015)
    opts.add_argument('--rule', type=str, default='pm25_12')
    opts.add_argument('--save', action='store_true')

    args = opts.parse_args()

    map_attainment_county(args.year, args.rule, save=args.save)
