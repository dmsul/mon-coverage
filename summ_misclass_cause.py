from econtools import state_fips_to_name, table_statrow, save_cli

from util.env import out_path
from analysis.basic_data import monitors_block
from analysis.misclass import blocks_misclass_flag
from analysis.monitor_sample import valid_naaqs_monitors


def block_has_valid_monitor(year: int):
    m_blocks = monitors_block()
    mons = valid_naaqs_monitors(year)
    mons['has_valid_mon'] = True
    mons = mons.set_index('monitor_id')[['has_valid_mon']]

    df = (m_blocks
          .to_frame()
          .join(mons, how='inner'))
    df = (df
          .drop_duplicates()
          .set_index('block_id'))

    assert df.index.is_unique

    return df


if __name__ == "__main__":
    year = 2014
    df = blocks_misclass_flag(year, 'pm25_12', 'msatna')
    df = df[~df['nonattain']].copy()

    df = df.drop(['has_mon_fips', 'fips_pop', 'has_over'], axis=1)
    fips_has_misclass = (df.groupby('fips')['is_over'].max()
                         .to_frame('fips_misclass'))
    print("Total misclass counties: {}".format(fips_has_misclass.sum()),
          flush=True)
    df = df.join(fips_has_misclass, on='fips')

    # Get monitor status
    has_valid_mon = block_has_valid_monitor(year)
    df = df.join(has_valid_mon)
    df['has_valid_mon'] = df['has_valid_mon'].fillna(False)
    fips_has_mon = df.groupby('fips')['has_valid_mon'].max()
    fips_has_mon.at['04027'] = True     # XXX hard fix for Yuma
    df = df.join(fips_has_mon.to_frame('fips_has_mon'), on='fips')

    misclass = df[df['fips_misclass']]

    fips_sums = misclass.groupby(['fips_has_mon', 'fips'])['pop'].sum()

    wut = fips_sums.reset_index()
    wut['state'] = wut['fips'].str[:2].astype(int).apply(state_fips_to_name)
    df = wut.groupby(['state', 'fips_has_mon'])['pop'].sum()

    df = (df
          .unstack('fips_has_mon')
          .fillna(0)
          .astype(int))
    df['total'] = df.sum(axis=1)
    df = df.sort_values('total')

    table_config = {'digits': 0, 'wrapnum': True}
    table_str = ''
    for name, row in df.iterrows():
        table_str += table_statrow(name, row.tolist(), **table_config)

    print(table_str)

    totals = df.sum(axis=0)
    print(totals)

    total_str = table_statrow("Total", totals.tolist(), **table_config)

    save = save_cli()
    if save:
        file_stem = 'summ_misclass_cause'
        with open(out_path(file_stem + '_main.tex'), 'w') as f:
            f.write(table_str)
        with open(out_path(file_stem + '_totals.tex'), 'w') as f:
            f.write(total_str)
