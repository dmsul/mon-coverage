import matplotlib.pyplot as plt

from econtools import legend_below, save_cli

from util import pmrule_imp_year
from util.env import out_path
from analysis.monitor_sample import (prep_monitor_analysis,
                                     semi_constant_monitor_panel,)


def pm_nonattainment_plot(rule='pm25_97', save=False):

    df = semi_constant_monitor_panel(rule=rule)
    df = prep_monitor_analysis(df, rule=rule)

    fig, ax = plt.subplots()

    df['cat'] = 0
    df.loc[df['targeted'], 'cat'] = 1
    df.loc[df['untargeted'], 'cat'] = 2
    df.loc[~df['nonattain'], 'cat'] = 3
    assert (df['cat'] > 0).min()

    cat_means = (df
                 .groupby(['cat', 'year'])['arithmetic_mean']
                 .mean()
                 .unstack('cat'))
    labels = {
        1: 'Monitors exceeding NAAQS in nonattainment areas (Group I)',
        2: ('Monitors not exceeding NAAQS in '
            'nonattainment areas (Group II)'),
        3: 'Monitors in attainment areas (Group III)'
    }
    colors = {
        1: 'maroon',
        2: 'orange',
        3: 'navy'
    }
    for cat in sorted(cat_means.columns):
        ax.plot(cat_means.index, cat_means[cat], '-o',
                label=labels[cat], color=colors[cat],
                zorder=9)

    ax.set_ylabel("Mean PM$_{2.5}$ ($\mu$g/m$^3$)")
    ax.set_yticks(list(range(7, 17)))
    ax.set_ylim(7, 16)
    ax.set_xlabel("Year")

    ax.yaxis.grid(color='gray', alpha=0.5, zorder=1)

    ax.axvline(pmrule_imp_year[rule],
               color='g', linestyle=':', zorder=2)

    horz_point = pmrule_imp_year[rule]
    vert_point = 15.01
    fontsize = 9

    ax.annotate(
        'Before',
        xy=(horz_point, vert_point),
        xytext=(-5, 0),
        textcoords='offset points',
        horizontalalignment='right',
        fontsize=fontsize,
    )

    arrow_shift_x_end = 0.8
    arrow_shift_x_start = .05
    arrow_shift_y = .2
    ax.annotate(
        '',
        xy=(horz_point - arrow_shift_x_end, vert_point - arrow_shift_y),
        xytext=(horz_point - arrow_shift_x_start, vert_point - arrow_shift_y),
        arrowprops=dict(facecolor='black', arrowstyle='->'))

    ax.annotate(
        'After\nNonattainment\nDesignation',
        xy=(horz_point, vert_point),
        xytext=(5, 0),
        textcoords='offset points',
        horizontalalignment='left',
        fontsize=fontsize,
    )
    ax.annotate(
        '',
        xy=(horz_point + arrow_shift_x_end, vert_point - arrow_shift_y),
        xytext=(horz_point + arrow_shift_x_start, vert_point - arrow_shift_y),
        arrowprops=dict(facecolor='black', arrowstyle='->'))

    legend_below(ax, ncol=1)

    if save:
        filepath = out_path(f'ts_monitor_attain_{rule}.pdf')
        fig.savefig(filepath, bbox_inches='tight', transparent=True)
        fig.savefig(filepath.replace('.pdf', '.png'),
                    bbox_inches='tight',
                    dpi=400,
                    transparent=True
                    )
        plt.close()
    else:
        plt.show()

    return df


if __name__ == '__main__':
    save = save_cli()

    df = pm_nonattainment_plot('pm25_12', save=save)
