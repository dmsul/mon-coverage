import matplotlib.pyplot as plt

from epa_airpoll.clean.census.xsection import load_block, block_id

from util.weighted_quantile import weighted_quantile
from analysis.blocks_monitor_dist import blocks_monitor_dist

from econtools import table_statrow

# data
df = blocks_monitor_dist()
x_sec = load_block()
x_sec.index = block_id(x_sec)  # create block_id and set equal to index


# histogram of `min_dist` variable
if 0:
    plt.hist(df['min_dist'], bins=15)
    plt.title("Minimum Distance to Monitor")
    plt.xlabel("Min. Dist. (km)")
    plt.ylabel("# of US Census Blocks")
    plt.show()


# deciles of `min_dist` variable
quantiles = [x / 10 for x in range(1, 10)]  # create quantiles
min_dist_quantiles = df['min_dist'].quantile(quantiles)


# deciles of pop-weighted `min_dist` variable
df_join = df.join(x_sec, how='left')  # merge datasets
df_join['population'].isnull().min()  # check for empty observations

# total population
total_pop_quantiles = weighted_quantile(df_join, 'min_dist', 'population',
                                        q=quantiles)

# white population
white_pop_quantiles = weighted_quantile(df_join, 'min_dist', 'race_white',
                                        q=quantiles)

# black population
black_pop_quantiles = weighted_quantile(df_join, 'min_dist', 'race_black',
                                        q=quantiles)

# distance to nearest monitor table
table_str = table_statrow("Total Population", total_pop_quantiles,
                          digits=2)

table_str += table_statrow("White Population", white_pop_quantiles,
                           digits=2)

table_str += table_statrow("Black Population", black_pop_quantiles,
                           digits=2)

print(table_str)
