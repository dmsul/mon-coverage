import cartopy.crs as ccrs
import cartopy.io.shapereader as shpreader
import matplotlib.pyplot as plt

from econtools import save_cli

from epa_airpoll import monitors_data, monitors_annual_summary

from util.env import gis_src_path, out_path
from analysis.monitor_sample import valid_naaqs_monitors


def prep_monitor_daycount(year):
    df = monitors_annual_summary(year)
    df = df[df['parameter_code'] == 88101]
    df = df[df['event_type'].isin(('No Events', 'Events Included'))]
    df = df[df['pollutant_standard'] == 'PM25 Annual 2006']
    df = df.set_index('monitor_id')['valid_day_count']

    return df


if __name__ == '__main__':
    # Plot state boundaries
    shp_path = gis_src_path('census', 'cb_2016_us_state_5m',
                            'cb_2016_us_state_5m.shp')
    adm_shapes = list(shpreader.Reader(shp_path).geometries())
    ax = plt.axes(projection=ccrs.PlateCarree())
    ax.add_geometries(adm_shapes, ccrs.PlateCarree(),
                      edgecolor='white', facecolor='0.9', linewidth=0.1,
                      zorder=1)
    ax.set_extent([-126, -66, 49.3, 24.8], ccrs.PlateCarree())
    ax.outline_patch.set_edgecolor('none')

    # Plot monitors
    year = 2015
    monitors = valid_naaqs_monitors(year)

    YEARS = range(year - 3, year)
    for y in YEARS:
        monitors = monitors.join(
            prep_monitor_daycount(2014).to_frame(f'vdc_{y}'),
            on='monitor_id')

    monitors['valid_day_count'] = (
        monitors[['vdc_{}'.format(x) for x in YEARS]]
        .median(axis=1))
    has_missing = (
        monitors[['vdc_{}'.format(x) for x in YEARS]]
        .isnull()
        .any(axis=1))
    monitors = monitors[~has_missing]
    x = monitors['longitude']
    y = monitors['latitude']
    if 0:
        ax.scatter(x, y, facecolor='red', edgecolor='none', zorder=9, s=.5,
                   marker='o')
    else:
        coverage_label = {1: '$\leq$80 days/year', 2: '81-120',
                          3: '121-300', 4: '>300'}
        coverage_colors = {1: 'red', 2: 'y', 3: 'navy', 4: 'green'}
        coverage_markers = {1: 'o', 2: '^', 3: 's', 4: 'p'}
        monitors['coverage_cat'] = -1
        monitors.loc[monitors['valid_day_count'] <= 80, 'coverage_cat'] = 1
        monitors.loc[(monitors['valid_day_count'] > 80) &
                     (monitors['valid_day_count'] <= 120), 'coverage_cat'] = 2
        monitors.loc[(monitors['valid_day_count'] > 120) &
                     (monitors['valid_day_count'] <= 300), 'coverage_cat'] = 3
        monitors.loc[monitors['valid_day_count'] > 300, 'coverage_cat'] = 4
        assert monitors['coverage_cat'].min() > 0

        for samp in sorted(monitors['coverage_cat'].unique()):
            ax.scatter(
                monitors.loc[monitors['coverage_cat'] == samp, 'longitude'],
                monitors.loc[monitors['coverage_cat'] == samp, 'latitude'],
                label=coverage_label[samp],
                facecolor=coverage_colors[samp],
                edgecolor='none',
                marker=coverage_markers[samp],
                s=1.25,
                zorder=9,
                alpha=.8,
            )

        legend = ax.legend(
            loc='lower right',
            fontsize='xx-small',
            labelspacing=0.25,
            handletextpad=0.3,
            handlelength=0.5,
        )
    save = save_cli()
    if save:
        plt.savefig(out_path('map_monitors_conus.pdf'),
                    bbox_inches='tight')
        plt.close()
    else:
        plt.show()
