from econtools import table_statrow, load_or_build

from epa_airpoll import (load_block, load_blockgroup,
                         nonattainment_block_panel)

from util.env import data_path, out_path
from analysis.basic_data import (prep_multisatpm_3year_wlag_block,
                                 prep_multisatpm_3year_wlag_bg,
                                 msatna_blocks_3lag_year,
                                 msatna_bg_3lag_year)
from analysis.misclass import blocks_misclass_flag


def misclass_demo(rule, nonatt_year, data, save=False):
    # block data (race and urban)
    block = _prep_block()
    df = _prep_data(block, rule, nonatt_year, data, 'block')

    digits = 1      # for table setup
    table_str = ''

    table_str += _a_row(df, 'population', 'Population', digits=digits)
    table_str += '\\\\\n'

    df['rural'] = df['population'] - df['urban']
    for urban in ('rural', 'urban'):
        table_str += _a_row(df, urban, urban.title(), digits=digits)

    table_str += "Race/Ethnicity\\\\\n"
    race_vars = ('race_white', 'race_black', 'race_hisp', 'race_asian',
                 'race_other')
    race_labels = ('White', 'Black', 'Hispanic', 'Asian', 'Other')
    for var, label in zip(race_vars, race_labels):
        table_str += _a_row(df, var, f'\\quad {label}', digits=digits)

    # block group data (income and education)
    block_group = _prep_bg()
    df = _prep_data(block_group, rule, nonatt_year, data, 'bg')

    table_str += "Education\\\\\n"
    educ_vars = ('no_hs_dip', 'hs_dip', 'coll_some', 'coll_plus')
    educ_labels = ("No H.S.~Diploma", "H.S.~Diploma", "Some College",
                   "College Degree or More")
    for var, label in zip(educ_vars, educ_labels):
        table_str += _a_row(df, var, f'\\quad {label}', digits=digits)

    table_str += "Household Income\\\\\n"
    income_vars = ('hhinc_bin_low', 'hhinc_bin_mid', 'hhinc_bin_high')
    income_labels = ("$<$\$35,000", "\$35,000--75,000", "$>$\$75,000")
    for var, label in zip(income_vars, income_labels):
        table_str += _a_row(df, var, f'\\quad {label}', digits=digits)

    if save:
        filepath = out_path(f'summ_nonatt_demo_{rule}_{nonatt_year}.tex')
        with open(filepath, 'w') as f:
            f.write(table_str)

    return table_str


def _a_row(df, colname, display_name, digits=1):
    var_total = df[colname].sum()
    frac_var = df.groupby('attain_cat')[colname].sum() / var_total
    frac_var[4] = frac_var[2] + frac_var[3]     # Total satellite nonattain
    frac_var[5] = frac_var[2] / frac_var[4]     # Failure rate of monitors
    frac_var *= 100                             # Scale to percent

    frac_var[6] = var_total / 1e6               # Population total

    table_frag = table_statrow(display_name, frac_var.tolist(),
                               digits=digits)

    return table_frag


def _prep_data(df, rule, nonatt_year, data, geounit):
    exp_year = nonatt_year

    # nonattainment
    nonatt = (nonattainment_block_panel(rule)[nonatt_year]
              .to_frame('nonatt'))
    if geounit == 'bg':
        nonatt.index = nonatt.index.str[:12]
        nonatt = nonatt.groupby(level=0).max()
    df = df.join(nonatt, how='left')

    df['nonatt'] = df['nonatt'].fillna(False)

    # exposure
    if data == 'multisatpm3':
        if geounit == 'block':
            exp = (
                prep_multisatpm_3year_wlag_block()[exp_year]
                .to_frame('exposure')
            )
        elif geounit == 'bg':
            exp = (
                prep_multisatpm_3year_wlag_bg()[exp_year]
                .to_frame('exposure')
            )

    elif data == 'msatna3':
        if geounit == 'block':
            exp = msatna_blocks_3lag_year(exp_year).to_frame('exposure')
        elif geounit == 'bg':
            exp = msatna_bg_3lag_year(exp_year).to_frame('exposure')

    else:
        raise ValueError

    df = df.join(exp, how='left')

    # Merge in geounit- and fips-level misclass
    # NOTE: We want to do this because we can't just grab the whole FIPS like
    # in `map_misclass` otherwise we'll double count non-attain sub-county
    # areas
    aux_df = blocks_misclass_flag(2014, 'pm25_12', 'msatna')
    if geounit == 'bg':
        aux_df['bg_id'] = aux_df.index.str[:12]
        aux_df = aux_df[~aux_df['nonattain']]
        bg_has_misclass = (aux_df.groupby('bg_id')['is_over'].max()
                           .to_frame('geounit_misclass'))
        df = df.join(bg_has_misclass)
        df['geounit_misclass'] = df['geounit_misclass'].fillna(False)
    else:
        df = df.join(
            aux_df['misclassed_block'].squeeze().to_frame('geounit_misclass'))

    # Calc fips-level misclass
    fips_has_misclass = df.groupby('fips')['geounit_misclass'].max()
    df = df.join(fips_has_misclass.to_frame('fips_misclass'), on='fips')
    df['fips_misclass'] = df['fips_misclass'].fillna(False)

    # XXX counties without exposure values dropped
    df = df[df['exposure'].notnull()]

    # CATEGORIES

    df['attain_cat'] = 0
    # attainment/below
    correct_attain = ((~df['nonatt']) &         # Attainment
                      (~df['fips_misclass'])    # No-one in county misclassed
                      )
    df.loc[correct_attain, 'attain_cat'] = 1
    # attainment/above
    misclassed = ((~df['nonatt']) &         # Attainment
                  (df['fips_misclass'])     # Someone in county misclassed
                  )
    df.loc[misclassed, 'attain_cat'] = 2
    # nonattainment/below
    df.loc[df['nonatt'], 'attain_cat'] = 3

    assert df['attain_cat'].isin([1, 2, 3]).min()

    return df

@load_or_build(data_path('tmp_demogs_block.pkl'))
def _prep_block():
    block = load_block()
    block['fips'] = (block['state'].astype(str).str.zfill(2) +
                     block['county'].astype(str).str.zfill(3))

    block['block_id'] = (
        block['state'].astype(str).str.zfill(2) +
        block['county'].astype(str).str.zfill(3) +
        block['tract'].astype(str).str.zfill(6) +
        block['block'].astype(str).str.zfill(4)
    )
    block = block.set_index('block_id')

    # Simplify race cats
    block['race_other'] += (block['race_native'] +
                            block['race_two_or_more'] +
                            block['race_island'])

    block = block[['fips', 'population', 'urban', 'race_white', 'race_black',
                   'race_asian', 'race_hisp', 'race_other',
                   'male', 'female']]

    return block

@load_or_build(data_path('tmp_demogs_bg.pkl'))
def _prep_bg():
    bg = load_blockgroup()

    bg['fips'] = (bg['state'].astype(str).str.zfill(2) +
                  bg['county'].astype(str).str.zfill(3))

    bg = bg.set_index(bg['state'].astype(str).str.zfill(2) +
                      bg['county'].astype(str).str.zfill(3) +
                      bg['tract'].astype(str).str.zfill(6) +
                      bg['block_group'].astype(str).str.zfill(1))
    bg.index.name = 'bg_id'

    # no highschool diploma
    bg['no_hs_dip'] = (
        bg['educ_male_0'] + bg['educ_male_4'] + bg['educ_male_6'] +
        bg['educ_male_8'] + bg['educ_male_9'] + bg['educ_male_10'] +
        bg['educ_male_11'] + bg['educ_male_12'] + bg['educ_female_0'] +
        bg['educ_female_4'] + bg['educ_female_6'] + bg['educ_female_8'] +
        bg['educ_female_9'] + bg['educ_female_10'] + bg['educ_female_11'] +
        bg['educ_female_12']
    )

    # highschool diploma
    bg['hs_dip'] = bg['educ_male_hs'] + bg['educ_female_hs']

    # some college
    bg['coll_some'] = (
        bg['educ_male_coll_1'] + bg['educ_male_coll_nod'] +
        bg['educ_male_aa'] + bg['educ_female_coll_1'] +
        bg['educ_female_coll_nod'] + bg['educ_female_aa']
    )

    # college degree or more
    bg['coll_plus'] = (
        bg['educ_male_ba'] + bg['educ_male_ma'] + bg['educ_male_jd'] +
        bg['educ_male_phd'] + bg['educ_female_ba'] + bg['educ_female_ma'] +
        bg['educ_female_jd'] + bg['educ_female_phd']
    )

    # household income below 30k
    bg['hhinc_bin_low'] = (
        bg['hhinc_0_10000'] +
        bg['hhinc_10000_14999'] +
        bg['hhinc_15000_19999'] +
        bg['hhinc_20000_24999'] +
        bg['hhinc_25000_29999'] +
        bg['hhinc_30000_34999']
    )

    # household income 30-59,999k
    bg['hhinc_bin_mid'] = (
        bg['hhinc_35000_39999'] +
        bg['hhinc_40000_44999'] +
        bg['hhinc_45000_49999'] +
        bg['hhinc_50000_59999'] +
        bg['hhinc_60000_74999']
    )

    # household income 60k or more
    bg['hhinc_bin_high'] = (
        bg['hhinc_75000_99999'] +
        bg['hhinc_100000_124999'] +
        bg['hhinc_125000_149999'] +
        bg['hhinc_150000_199999'] +
        bg['hhinc_200000_plus']
    )

    bg = bg[['fips', 'no_hs_dip', 'hs_dip', 'coll_some', 'coll_plus',
             'pop_25_plus', 'population', 'hhinc_total', 'hhinc_bin_low',
             'hhinc_bin_mid', 'hhinc_bin_high']]

    return bg


if __name__ == '__main__':
    import argparse

    opts = argparse.ArgumentParser()
    opts.add_argument('--rule', type=str, default='pm25_12')
    opts.add_argument('--year', type=int, default=2015)
    opts.add_argument('--data', type=str, default='msatna3',
                      choices=['multisatpm3', 'msatna3'])
    opts.add_argument('--save', action='store_true')

    args = opts.parse_args()

    table_str = misclass_demo(args.rule, args.year, args.data, save=args.save)
    print(table_str)
