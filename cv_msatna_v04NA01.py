import numpy as np
import matplotlib.pyplot as plt

from multisatpm import msat_northamer_conus_3year
from epa_airpoll.clean.raw import monitors_annual_summary

from util.env import out_path


def main(year, save=False):
    pm = prep_msatna(year)
    mon = prep_mon(year)
    mon = mon.join(pm['pm25'], how='inner', on=['x_int', 'y_int'])

    fig, ax = plt.subplots()

    for samp in mon['Sample Duration'].unique():
        ax.scatter(
            mon.loc[mon['Sample Duration'] == samp, 'pm25'],
            mon.loc[mon['Sample Duration'] == samp, 'mean'],
            label=samp,
            alpha=.25,
        )
    mon['expint'] = np.around(mon['pm25'])
    binscatter = mon.groupby('expint')['mean'].mean()
    ax.scatter(binscatter.index, binscatter, label='Mean', color='k')

    space = np.linspace(mon['mean'].min(), mon['mean'].max(), 100)
    ax.plot(space, space, color='k')

    ax.grid(True, alpha=0.5, color='gray')

    ax.set_ylabel("Monitor")
    ax.set_xlabel("Satellite")

    ax.legend(loc='upper left')

    if save:
        ax.set_title(f'{year}')
        fig.savefig(out_path(f'cv_msatna_{year}.png'),
                    bbox_inches='tight',
                    dpi=500)
        plt.close()
    else:
        plt.show()


def prep_msatna(year):
    df = msat_northamer_conus_3year(year)
    df.name = 'pm25'
    df = df.reset_index()

    df['x_int'] = xy_as_int(df['x'])
    df['y_int'] = xy_as_int(df['y'])

    df = df.set_index(['x_int', 'y_int'])

    return df


def prep_mon(yearT):
    df = _prep_mon_year(yearT)

    df['mean'] += _prep_mon_year(yearT - 1)['mean']
    df['mean'] += _prep_mon_year(yearT - 2)['mean']

    df['mean'] /= 3

    return df


def _prep_mon_year(year):
    df = monitors_annual_summary(year)
    df = df[df['Parameter Code'] == 88101].drop_duplicates('monitor_id')

    df['x_int'] = xy_as_int(df['longitude'])
    df['y_int'] = xy_as_int(df['latitude'])

    df = df.rename(columns={'arithmetic_mean': 'mean'})

    df = df.set_index('monitor_id')
    df = df[['mean', 'x_int', 'y_int', 'sample_duration']]

    return df


def xy_as_int(x, pm=False):
    return np.around(x * 100).astype(int)


if __name__ == '__main__':
    for y in range(2005, 2013):
        print(y)
        main(y, save=True)
